//
// Created by reinis on 02.03.21.
//
#include "../pipeline/ops/BoardLengthMeasurement.h"
#include "../pipeline/ops/WatershedBoardLengthMeasurement.h"
#include "../pipeline/ops/Rectification.h"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <vector>
#include <opencv2/imgproc.hpp>

using namespace omcam::img;
int main(int argc, char* argv[]) {
    omcam::img::board_length_measurement_parameters_t params = {
            .threshold_min=50,
            .roi = cv::Rect2i(0, 0, 1296, 248),

            .sf_length_poly_a0 = 7.80551286e+02,
            .sf_length_poly_a1 = 6.05772145e+00,
            .sf_length_poly_a2 = -3.26553064e-03,
            .sf_length_poly_a3 = 1.54930219e-06,

            .sf_width_poly_a0 = 1.84515993e+01,
            .sf_width_poly_a1 = 3.33074439e+00,
            .sf_width_poly_a2 = -6.28582930e-03,
            .sf_width_poly_a3 = 0.0f,
    };

/*    auto opRectification = std::make_shared<omcam::img::Rectification>("rectification", "");
    opRectification->loadParameters(IMAGE_SRC_DIR "/images/camera_calibration.yml");
    */
    auto opBoardMeasurement = WatershedBoardLengthMeasurement("boardlen", params);

    std::vector<std::string> images;


//    images.emplace_back(IMAGE_SRC_DIR "/images/dark-22-11-2023/capture_2023_11_22_08_09_1467723_0mm.png");
//    images.emplace_back(IMAGE_SRC_DIR "/images/dark-22-11-2023/capture_2023_11_22_14_47_152951_3401mm.png");
    images.emplace_back(IMAGE_SRC_DIR "/images/rot-2024/capture_11-25_46712_4680mm.png");
    images.emplace_back(IMAGE_SRC_DIR "/images/rot-2024/capture_11-25_46713_4721mm.png");

    for (auto& image_fn: images) {
        auto inputMat = cv::imread(image_fn, cv::IMREAD_COLOR);
        cv::Point2f cp(inputMat.cols/2.0f, inputMat.rows/2.0f);
        float rotAngle = -1.95f;
        auto rotMat = cv::getRotationMatrix2D(cp, rotAngle, 1.0f);
        cv::Mat inputMatRotated;
        cv::warpAffine(inputMat, inputMatRotated, rotMat, inputMat.size());
        auto inputPtr = std::make_shared<cv::Mat>(inputMatRotated);
        if (inputPtr->data != nullptr) {
            std::cout << "Image: " << image_fn << std::endl;

            std::shared_ptr<cv::Mat> rectified = std::make_shared<cv::Mat>();
            auto outputPtr = std::make_shared<cv::Mat>();
            //opRectification->execute(inputPtr, rectified);
            opBoardMeasurement.execute(inputPtr, outputPtr);
            auto results = opBoardMeasurement.getResults();

            //cv::imshow("rectified", *input);
            //cv::waitKey(0);
            std::cout << "Boards detected: " << results.boards.size() << std::endl;
            for (auto &b: results.boards) {
                std::cout << "Board length px=" << b.board_length_px << ", mm=" << b.board_length_mm << std::endl;
                std::cout << "Board width px=" << b.board_width_px << ", mm=" << b.board_width_mm << std::endl;
            }
            cv::imshow("corners", results.corners_dbg_mat);
            while (cv::waitKey(1) != 'q'){}
        } else {
            std::cerr << "Could not open image: '" << image_fn << "'" << std::endl;
            return -1;
        }
    }
}