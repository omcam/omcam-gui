#include "hal/base/distance-sensor.h"

DistanceSensor::DistanceSensor() : 
    _ranging_started(false)
{
    _log = spdlog::get("omcam-distance-sensor");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-distance-sensor");
    }

}
