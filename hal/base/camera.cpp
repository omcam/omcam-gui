#include "hal/base/camera.h"

Camera::Camera() :
    _width(0),
    _height(0)
{
    _log = spdlog::get("omcam-camera");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-camera");
    }
}
