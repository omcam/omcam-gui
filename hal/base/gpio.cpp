#include "hal/base/gpio.h"


using namespace omcam::hal;
GPIO::GPIO() : 
    _hw_input_state(),
    _input_is_forced(),
    _forced_input_state(),
    _hw_output_state(),
    _output_is_forced(),
    _forced_output_state(),
    _prev_input_state()
{
    _log = spdlog::get("omcam-gpio");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-gpio");
    }
}

bool GPIO::toggle_output_state(output_t out)
{
    ASSERT_OUTPUT_VALID(out);
    if (is_forcing(out)) {
        return true;
    }

    _log->debug("Toggling output {}", out);
    if (_hw_output_state[out] == LOW)
        return set_output_state(out, HIGH);
    else
        return set_output_state(out, LOW);
}

io_state_t GPIO::get_output_state(output_t out)
{
    ASSERT_OUTPUT_VALID(out);
    return _hw_output_state[out];
}

bool GPIO::is_forcing(output_t out)
{
    ASSERT_OUTPUT_VALID(out);
    return _output_is_forced[out];
}

bool GPIO::is_forcing(input_t in)
{
    ASSERT_INPUT_VALID(in);
    return _input_is_forced[in];
}

void GPIO::stop_forcing(output_t out)
{
    ASSERT_OUTPUT_VALID(out);
    _log->debug("stopping forcing of output: {}", out);
    set_output_state(out, _hw_output_state[out]);
    _output_is_forced[out] = false;
}

void GPIO::stop_forcing(input_t in)
{
    ASSERT_INPUT_VALID(in);
    _log->debug("stopping forcing of input: {}", in);
    _input_is_forced[in] = false;
}

void GPIO::start_forcing(output_t out, io_state_t state)
{
    ASSERT_OUTPUT_VALID(out);

    _log->debug("starting forcing of output: {}, state: {}", out, state);
    _output_is_forced[out] = true;
    _forced_output_state[out] = state;
    set_output_state(out, state);
}

void GPIO::start_forcing(input_t in, io_state_t state)
{
    ASSERT_INPUT_VALID(in);

    _log->debug("starting forcing of input: {}, state: {}", in, state);
    _input_is_forced[in] = true;
    _forced_input_state[in] = state;
}

io_state_t GPIO::get_forcing_state(output_t out)
{
    ASSERT_OUTPUT_VALID(out);
    return _forced_output_state[out];
}

io_state_t GPIO::get_forcing_state(input_t in)
{
    ASSERT_INPUT_VALID(in);
    return _forced_input_state[in];
}

bool GPIO::get_input_posedge(input_t input) {
    auto current_state = get_input_state(input);
    bool ret = false;
    if (current_state == HIGH && _prev_input_state[input] == LOW)
        ret = true;
    _prev_input_state[input] = current_state;
    return ret;
}

bool GPIO::get_input_negedge(input_t input) {
    auto current_state = get_input_state(input);
    bool ret = false;
    if (current_state == LOW && _prev_input_state[input] == HIGH)
        ret = true;
    _prev_input_state[input] = current_state;
    return ret;
}
