#pragma once

#include <stdint.h>
#include <opencv2/core.hpp>
#include <utility>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <QObject>

class Camera: public QObject {
    Q_OBJECT
public:
    Camera();
    virtual bool setup(unsigned int width, unsigned int height) = 0;
    virtual bool open() = 0;
    unsigned int getWidth() const { return _width; }
    unsigned int getHeight() const { return _height; }
    
    //synchronous image capture
    virtual bool captureImage(cv::Mat& mat) = 0;

    virtual bool isOpened() const = 0;
    virtual void close() {}
protected:
    unsigned int _width;
    unsigned int _height;
    std::shared_ptr<spdlog::logger> _log;
};

Q_DECLARE_INTERFACE(Camera, "Camera")
