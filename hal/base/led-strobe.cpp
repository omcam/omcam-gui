#include "hal/base/led-strobe.h"

LEDStrobe::LEDStrobe() : 
    _brightness(0.0f),
    _enabled(false)
{
    _log = spdlog::get("omcam-led-strobe");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-led-strobe");
    }
}


bool LEDStrobe::toggle(void) 
{
    if (_enabled)
        return disable();
    else
        return enable();
}
