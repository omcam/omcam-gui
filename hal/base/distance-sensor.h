#pragma once

#include <stdint.h>
#include <stdbool.h>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

class DistanceSensor {
public:
    DistanceSensor();
    virtual bool open(uint8_t addr, const char* bus) = 0;
    
    typedef enum {
        STATUS_RANGE_VALID,
        STATUS_RANGE_INVALID,
        STATUS_ERROR
    } range_status_t;
    
    virtual bool start_continuous_ranging(void) = 0;
    virtual bool stop_continuous_ranging(void) = 0;
    virtual bool wait_measurement(void) = 0;
    virtual bool continuous_ranging_started() { return _ranging_started; }
    virtual range_status_t read_range(float& range_mm) = 0;

protected:
    std::shared_ptr<spdlog::logger> _log;
    bool                            _ranging_started;
};
