#pragma once

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

class LEDStrobe {
    public:
        LEDStrobe();
        virtual bool initialize(void) = 0;
        virtual bool enable(void) = 0;
        virtual bool disable(void) = 0;
        virtual bool toggle(void);
        virtual bool set_brightness(float brightness) { _brightness = brightness; return true; }
    protected:
        std::shared_ptr<spdlog::logger> _log;
        float _brightness;
        bool _enabled;
};
