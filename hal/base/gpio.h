#pragma once

/***
 *  GPIO module is responsible for user-accessible IO channels.
 *
 *  The basic interface is:
 *  GPIO::set_output_state
 *  GPIO::get_output_state
 *  GPIO::get_input_state
 *
 *  Concept of forcing:
 *  For testing the functionality of hardware/externally connected hardware, it is convenient to be able
 *  to force the state of outputs or inputs, irrespective of what the rest of the OMCam is doing with the IOs.
 *
 *  Once forcing of output is started, any set_output_state requests are ignored and output is fixed at forced state.
 *  get_output_state returns the forced state.
 *
 *  Once forcing of input is started, any changes in corresponding hardware input is ignored and get_input_state returns the forced value.
 *
 *
 */


#include <stdbool.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "core/oassert.h"

namespace omcam {
    namespace hal {

typedef enum {
    IN0 = 0,
    IN1,
    IN_END
} input_t;

typedef enum {
    OUT0 = 0,
    OUT1,
    OUT_LED_USR,
    OUT_LED_ERR,
    OUT_END
} output_t;

typedef enum {
    LOW = 0,
    HIGH,
    UNINITIALIZED
} io_state_t;

static const char* INPUT_NAMES[] =
        {"In0", "In1", NULL};

static const char* OUTPUT_NAMES[] =
        {"Out0", "Out1", "OutLEDUsr", "OutLedErr", NULL};

#define ASSERT_OUTPUT_VALID(o) oassert((o >= 0 && (o < OUT_END)), "Invalid output")
#define ASSERT_INPUT_VALID(i) oassert((i >= 0 && (i < IN_END)), "Invalid input")


class GPIO {
    public:
        GPIO();
       
        /**
         * Responsibilities of initialize:
         * 1) Initialize hardware
         * 2) Initialize the _hw_output/input_state to correct values
         */
        virtual bool initialize(void) = 0;

        /* Responsibilities of set_output_state:
         * 0) Assert that given output is valid (ASSERT_OUTPUT_VALID)
         * 1) Set the corresponding IO state to requested value,
         * 2) Update the _hw_output_state
         * 3) Obey the forced state
         */
        virtual bool set_output_state(output_t output, io_state_t state) = 0;

        /* Responsibilities of get_input_state:
         * 0) Assert that given input is valid (ASSERT_INPUT_VALID)
         * 1) Get the correct value from IO pin
         * 2) Update the _hw_input_state
         * 3) Obey the forced state
         */
        virtual io_state_t get_input_state(input_t input) = 0;

        /**
         * Returns true if input state has changed from LOW to HIGH between the calls
         * to this function
         * @param input
         * @return
         */
        bool get_input_posedge(input_t input);

        /**
         * Returns true if input state has changed from HIGH to LOW between the calls
         * to this function
         * @param input
         * @return
         */
        bool get_input_negedge(input_t input);
        virtual bool toggle_output_state(output_t output);
        virtual io_state_t get_output_state(output_t output);

        virtual bool is_forcing(output_t output);
        virtual bool is_forcing(input_t input);
        virtual void stop_forcing(output_t output);
        virtual void stop_forcing(input_t output);
        virtual void start_forcing(input_t input, io_state_t state);
        virtual void start_forcing(output_t output, io_state_t state);
        virtual io_state_t get_forcing_state(output_t output);
        virtual io_state_t get_forcing_state(input_t input);

    protected:
        std::shared_ptr<spdlog::logger> _log;

        io_state_t _hw_input_state[IN_END];
        bool       _input_is_forced[IN_END];
        io_state_t _forced_input_state[IN_END];
        io_state_t _prev_input_state[IN_END];
        io_state_t _hw_output_state[OUT_END];
        bool       _output_is_forced[OUT_END];
        io_state_t _forced_output_state[OUT_END];
};

}}//namespace omcam::hal
