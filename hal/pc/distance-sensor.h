#pragma once

#include "hal/base/distance-sensor.h"

class DistanceSensorPc : public DistanceSensor {

public:
    DistanceSensorPc() : DistanceSensor() {}
    bool open(uint8_t addr, const char* bus) { return false; }
    
    bool start_continuous_ranging(void) { return false; }
    bool stop_continuous_ranging(void) { return false; }
    bool wait_measurement(void) { return false; }
    bool continuous_ranging_started() { return _ranging_started; }
    DistanceSensor::range_status_t read_range(float& range_mm) { return DistanceSensor::STATUS_ERROR; }

protected:
    bool                            _ranging_started;
    
};
