#pragma once

#include <opencv2/videoio.hpp>
#include "hal/base/camera.h"
#include <opencv2/videoio.hpp>
#include "vimba_image_capture.h"

class CameraPcVimba : public Camera {
    Q_OBJECT
    Q_INTERFACES(Camera)
public:
    CameraPcVimba(unsigned int idx = 0);

    bool setup(unsigned int width, unsigned int height) override;
    bool open() override;

    //synchronous image capture
    bool captureImage(cv::Mat& mat) override;
    bool isOpened() const override {
        return _imageCapture != nullptr && _imageCapture->isOpened();
    }
    void close() override;
protected:
    unsigned int     _camera_index;
    std::shared_ptr<VimbaCapture>    _imageCapture;
};
