#include "hal/pc/gpio.h"

using namespace omcam::hal;

GPIOPc::GPIOPc() : GPIO() 
{

}

bool GPIOPc::initialize(void) 
{
    _log->info("GPIOPc initialized");
    return true;
}

bool GPIOPc::set_output_state(output_t output, io_state_t state)
{
    ASSERT_OUTPUT_VALID(output);
    if (_output_is_forced[output]) 
    {
        _log->info("not setting output {} to state: {} because it's being forced");
        return true;
    }

    _log->info("setting output: {}, state: {}", output, state);
    _hw_output_state[output] = state;
    return true;
}


io_state_t GPIOPc::get_input_state(input_t input)
{
    ASSERT_INPUT_VALID(input);
    if (_input_is_forced[input]) {
        _log->debug("returning forced input state for {}", input);
        return _forced_input_state[input];
    }

    return LOW;
}
