#ifndef VIMBAFRAMEOBSERVER_H
#define VIMBAFRAMEOBSERVER_H

#include <VimbaCPP/Include/VimbaCPP.h>
#include <queue>
#include <QObject>
#include <QMutex>

class VimbaFrameObserver : public QObject, virtual public AVT::VmbAPI::IFrameObserver
{
    Q_OBJECT
public:
    VimbaFrameObserver(AVT::VmbAPI::CameraPtr camera_ptr):
        AVT::VmbAPI::IFrameObserver(camera_ptr)
    {

    }

    virtual void FrameReceived(AVT::VmbAPI::FramePtr pFrame);

    AVT::VmbAPI::FramePtr GetFrame();

    // Clears the double buffer frame queue
    void ClearFrameQueue();

public slots:

private:
    // the frame observer stores all FramePtr
    std::queue<AVT::VmbAPI::FramePtr> m_Frames;
    QMutex m_FramesMutex;
    signals:
    // The frame received event that passes the frame directly
    void FrameReceivedSignal( int status );
};

#endif // VIMBAFRAMEOBSERVER_H
