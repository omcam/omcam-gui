#pragma once

#include <opencv2/videoio.hpp>
#include "hal/base/camera.h"
#include <opencv2/videoio.hpp>

class CameraPc : public Camera {
    Q_OBJECT
    Q_INTERFACES(Camera)
public:
    CameraPc(unsigned int idx = 0);

    bool setup(unsigned int width, unsigned int height) override;
    bool open() override;
    
    //synchronous image capture
    bool captureImage(cv::Mat& mat) override;
    bool isOpened() const override {
        return _vc.isOpened();
    }
protected:
    unsigned int _camera_index;
    cv::VideoCapture    _vc;
};
