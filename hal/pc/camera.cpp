#include "hal/pc/camera.h"


CameraPc::CameraPc(unsigned int idx) :
    Camera(),
    _camera_index(idx),
    _vc(idx)
{
    _log->info("using cv::VideoCapture camera idx={}", idx);
}

bool CameraPc::setup(unsigned int width, unsigned int height)
{
    _width = width;
    _height = height;
    _vc.set(cv::CAP_PROP_FRAME_WIDTH, width);
    _vc.set(cv::CAP_PROP_FRAME_HEIGHT, height);

    _log->info("setup done, w={}, h={}", width, height);
    return true;
}

bool CameraPc::open()
{
    bool ret = _vc.isOpened();
    _log->info("cv::VideoCapture open result={}", ret);
    return ret;
}

bool CameraPc::captureImage(cv::Mat& mat)
{
    if (_vc.isOpened()) {
        _vc >> mat;
        return true;
    } else {
        return false;
    }
}
