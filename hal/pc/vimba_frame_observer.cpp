#include "vimba_frame_observer.h"
#include <iostream>
using namespace AVT;
using namespace AVT::VmbAPI;


void VimbaFrameObserver::FrameReceived( const FramePtr pFrame )
{
    bool bQueueDirectly = true;
    VmbFrameStatusType eReceiveStatus;
    if(     0 != receivers(SIGNAL(FrameReceivedSignal(int)) )
        &&  VmbErrorSuccess == pFrame->GetReceiveStatus( eReceiveStatus ) )
    {
        // Lock the frame queue
        m_FramesMutex.lock();
        // Add frame to queue
        m_Frames.push( pFrame );
        // Unlock frame queue
        m_FramesMutex.unlock();
        // Emit the frame received signal
        emit FrameReceivedSignal( eReceiveStatus );
        bQueueDirectly = false;
    }

    // If any error occurred we queue the frame without notification
    if( true == bQueueDirectly )
    {
        m_pCamera->QueueFrame( pFrame );
    }
}

// Returns the oldest frame that has not been picked up yet
FramePtr VimbaFrameObserver::GetFrame()
{
    // Lock the frame queue
    m_FramesMutex.lock();
    // Pop frame from queue
    FramePtr res;
    if( !m_Frames.empty() )
    {
        res = m_Frames.front();
        m_Frames.pop();
    }
    // Unlock frame queue
    m_FramesMutex.unlock();
    return res;
}

void VimbaFrameObserver::ClearFrameQueue()
{
    // Lock the frame queue
    m_FramesMutex.lock();
    // Clear the frame queue and release the memory
    std::queue<FramePtr> empty;
    std::swap( m_Frames, empty );
    // Unlock the frame queue
    m_FramesMutex.unlock();
}
