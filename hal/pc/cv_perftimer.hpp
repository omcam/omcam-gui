//
// Created by reinis on 11/16/23.
//

#ifndef OMCAM_ACQUISITION_CV_PERFTIMER_HPP
#define OMCAM_ACQUISITION_CV_PERFTIMER_HPP

#include <opencv2/core/core.hpp>
//Small and simple performance timer
class PerfTimer {

    uint64_t start_ticks;

public:
    void start() {
        start_ticks = cv::getTickCount();
    }
    double end_ms() {
        return end_s() * 1000.0f;
    }

    double end_s() {
        return (cv::getTickCount() - start_ticks) / cv::getTickFrequency();
    }
};



#endif //OMCAM_ACQUISITION_CV_PERFTIMER_HPP
