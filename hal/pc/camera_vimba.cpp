//
// Created by reinis on 11/16/23.
//
#include "hal/pc/camera_vimba.h"


CameraPcVimba::CameraPcVimba(unsigned int idx) :
        Camera(),
        _camera_index(idx)
{
    _log->info("using VimbaCapture camera idx={}", idx);
}

bool CameraPcVimba::setup(unsigned int width, unsigned int height)
{
    _width = width;
    _height = height;

    _log->info("setup done, w={}, h={}", width, height);
    return true;
}

bool CameraPcVimba::open()
{
    if (_imageCapture)
        _imageCapture.reset();

    _imageCapture = std::make_shared<VimbaCapture>();

    image_capture_settings_t s;

    s.acquisition_mode = ACQUISITION_HW_TRIGGER;
    s.binning = BINNING_2X;
    s.exposure_mode = EXPOSURE_FIXED;
    s.gain_mode = GAIN_FIXED;
    s.gain_fixed = 10;
    s.camera_index = 0;
    s.aperture = 50;
    s.brightness = 50;
    s.dsp_bottom = 0;
    s.dsp_left = 0;
    s.dsp_top = 0;
    s.dsp_right = 0;
    s.hw_trigger_delay_us = 0;
    s.shutter_speed = 0.5f;
    s.resolution_x = _width;
    s.resolution_y = _height;
    s.offset_x = 0;
    s.offset_y = 352;
    s.fps = 10.0f;

    _imageCapture->init(s);
    bool ret = _imageCapture->isOpened();
    _log->info("VimbaCapture open result={}", ret);
    return ret;
}

bool CameraPcVimba::captureImage(cv::Mat& mat)
{
    return _imageCapture->getFrame(mat);
}

void CameraPcVimba::close() {
    if (_imageCapture != nullptr)
        _imageCapture.reset();
}
