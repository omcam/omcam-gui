#pragma once

/** 
 * This implementation of GPIO for PC just logs the outputs and returns 0 for inputs
 */

#include "hal/base/gpio.h"

namespace omcam {
namespace hal {

class GPIOPc : public GPIO {
    public:
        GPIOPc();
        bool initialize(void) override;
        bool set_output_state(output_t output, io_state_t state) override;
        io_state_t get_input_state(input_t input) override;
};




}} // namespace omcam::hal
