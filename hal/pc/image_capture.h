//
// Created by reinis on 11/16/23.
//

#ifndef OMCAM_ACQUISITION_IMAGE_CAPTURE_H
#define OMCAM_ACQUISITION_IMAGE_CAPTURE_H
#pragma once

#include <QObject>

#include <opencv2/imgproc/imgproc.hpp>

#include <string>
#include <stdio.h>
#include <stdarg.h>

using namespace cv;

typedef enum {
    ACQUISITION_CONTINUOUS = 0,
    ACQUISITION_HW_TRIGGER,
    ACQUISITION_SW_TRIGGER,
    ACQUISITION_RECORDER_MODE
} acquisition_mode_t;

typedef enum {
    BINNING_NONE = 1,
    BINNING_2X = 2,
    BINNING_4X = 4
} binning_mode_t;

typedef enum {
    EXPOSURE_FIXED = 0,
    EXPOSURE_AUTO
} exposure_mode_t;
typedef enum {
    GAIN_AUTO=0,
    GAIN_FIXED
} gain_mode_t;
typedef struct {
    //0..100%
    int iso;
    //0..100% -> 0..33ms
    float shutter_speed;

    //hardware trigger delay, us
    float hw_trigger_delay_us;
    //ignored for now
    int aperture;

    //in pixels
    int resolution_x;
    int resolution_y;
    int offset_x;
    int offset_y;

    int dsp_top;
    int dsp_bottom;
    int dsp_left;
    int dsp_right;

    int brightness;
    //degrees, CW
    int rotation;
    //ignored on some platforms
    int camera_index;
    binning_mode_t binning;
    acquisition_mode_t acquisition_mode;
    exposure_mode_t exposure_mode;
    gain_mode_t     gain_mode;
    float           gain_fixed;
    float fps;

} image_capture_settings_t;

class CameraInfo {
public:
    int         camera_index;
    std::string identifier;
    std::string name;
    std::string model;
    std::string serial_number;
};
/**
 * Abstract class to be used as image capture interface.
 * Implementations should exist for OpenCV VideoCapture backend
 * and RaspberryPi camera.
 *
 */
class ImageCaptureInterface: public QObject {
Q_OBJECT
public:
    virtual unsigned int  getCameraCount() = 0;
    virtual bool getCameraInfo(unsigned int index, CameraInfo& info) = 0;

    virtual bool init(const image_capture_settings_t &parameters) = 0;

    /**
     * @brief Synchronous image capture
     * @param image
     * @return capture successful
     */
    virtual bool capture(Mat &image) = 0;
    bool isOpened() { return _is_opened; };

    virtual bool updateSettings(const image_capture_settings_t &parameters) = 0;

    virtual bool getFrame(Mat& frame) = 0;
    virtual bool trigger(void) = 0;

    virtual bool setOutputState(bool state)
    {
        (void)state;
        return false;
    }
    virtual bool getOutputState(bool& state)
    {
        (void)state;
        return false;
    }

signals:
    void frameReceivedSignal(int status);

protected:
    bool _is_opened;
};

Q_DECLARE_INTERFACE(ImageCaptureInterface, "ImageCaptureInterface")

#endif //OMCAM_ACQUISITION_IMAGE_CAPTURE_H
