#include "vimba_image_capture.h"
#include "cv_perftimer.hpp"
#include <VimbaCPP/Include/VimbaCPP.h>
#include <VimbaCPP/Include/Feature.hpp>

#include <iostream>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_sinks.h>

using namespace AVT;
using namespace AVT::VmbAPI;

//@TODO: Need to re-work the observer interface, as this capture backend cannot be observer
VimbaCapture::VimbaCapture() :
    sys(VimbaSystem::GetInstance()),
    m_sensor_height(0),
    m_sensor_width(0),
    m_sensor_height_max(0),
    m_sensor_width_max(0)
{
    log = spdlog::stdout_logger_st("vimba");
	sys.Startup();
    _is_opened = false;
}

VimbaCapture::~VimbaCapture()
{
    log->info("shutting down Vimba");
	sys.Shutdown();
}


bool VimbaCapture::init(const image_capture_settings_t &parameters) {
    settings = parameters;

    //try to open camera by index
    CameraPtrVector cameras;
    sys.GetCameras(cameras);
    
    for (unsigned int i = 0; i < 5; i++)
    {
        FramePtr obj;
        m_frame_ptrs.push_back(obj);
    }
    if ((unsigned int)parameters.camera_index >= cameras.size()) {
        log->error("Out of bounds camera index (requested: {}, num cameras: {}", parameters.camera_index, cameras.size());
        //out of bounds
        return false;
    }

    if (cameras[parameters.camera_index]->Open(VmbAccessModeFull) == VmbErrorSuccess) {
        camera = cameras[parameters.camera_index];
        std::string camera_id = "unknown";
        camera->GetID(camera_id);
        log->info("opened vimba camera, id: {}", camera_id);
        if (prepareCamera()) {

            frame_observer = new VimbaFrameObserver(camera);
            SP_SET(frame_observer_ptr, frame_observer);

            QObject::connect(frame_observer, SIGNAL(FrameReceivedSignal(int)),this,SLOT(observerFrameReceived(int)));
            log->info("camera settings written successfully");
            return updateSettings(settings);
        } else {
            log->error("failed to prepare camera settings");
        }
    } else {
        log->error("camera->Open failed!");
    }

    return false;
}

bool VimbaCapture::updateSettings(const image_capture_settings_t &parameters) {
    settings = parameters;
    FeaturePtr feature;

    stopCapture();

    if (!readSensorInfo()) {
        log->error("Failed to read sensor info");
        return false;
    }

    if (!setFeatureValue("PixelFormat", "Mono8"))
        return false;

    if (!setFeatureValue("BinningHorizontalMode", "Average")) {
        return false;
    }
    if (!setFeatureValue("BinningVerticalMode", "Average")) {
        return false;
    }
    if (!setFeatureValue("BinningHorizontal", (int)settings.binning)) {
        return false;
    }
    if (!setFeatureValue("BinningVertical", (int)settings.binning)) {
        return false;
    }

    if (settings.resolution_x % 8 != 0)
        log->warn("resolution x is not divisible by 8");
    if (settings.resolution_y % 8 != 0)
        log->warn("resolution y is not divisible by 8");

    if (settings.offset_x % 8 != 0)
        log->warn("offset x is not divisible by 8");

    if (settings.offset_y % 8 != 0)
        log->warn("offset y is not divisible by 8");

    if (!setFeatureValue("Width", settings.resolution_x))
        return false;
    if (!setFeatureValue("Height", settings.resolution_y))
        return false;
    if (!setFeatureValue("OffsetX", settings.offset_x))
        return false;
    if (!setFeatureValue("OffsetY", settings.offset_y))
        return false;
    if (settings.dsp_right == 0 && settings.dsp_bottom == 0 && settings.dsp_top == 0 && settings.dsp_left == 0) {
        log->info("skipping setting DSP region");
    } else {
        if (settings.dsp_bottom == 0 || (unsigned int) settings.dsp_bottom > m_sensor_height_max) {
            settings.dsp_bottom = m_sensor_height_max;
        }
        if (settings.dsp_right == 0 || (unsigned int) settings.dsp_right > m_sensor_width_max) {
            settings.dsp_right = m_sensor_width_max;
        }

        if (featureExists("AutoModeRegionOffsetY")) {
            //newer cameras only have AutoModeRegion, older cameras had DSPSubregion
            //first set the region width & height to zero, so that top can be set
            setFeatureValue("AutoModeRegionHeight", 0);
            setFeatureValue("AutoModeRegionWidth", 0);
            if (!setFeatureValue("AutoModeRegionOffsetY", settings.dsp_top))
                return false;
            if (!setFeatureValue("AutoModeRegionHeight", settings.dsp_bottom - settings.dsp_top))
                return false;
            if (!setFeatureValue("AutoModeRegionOffsetX", settings.dsp_left))
                return false;
            if (!setFeatureValue("AutoModeRegionWidth", settings.dsp_right - settings.dsp_left))
                return false;
        } else {
            setFeatureValue("DSPSubregionBottom", (int32_t) m_sensor_height_max);
            setFeatureValue("DSPSubregionRight", (int32_t) m_sensor_width_max);

            if (!setFeatureValue("DSPSubregionTop", settings.dsp_top))
                return false;
            if (!setFeatureValue("DSPSubregionBottom", settings.dsp_bottom))
                return false;
            if (!setFeatureValue("DSPSubregionLeft", settings.dsp_left))
                return false;
            if (!setFeatureValue("DSPSubregionRight", settings.dsp_right))
                return false;
        }
    }


    if (settings.exposure_mode == EXPOSURE_AUTO)
    {
        if (!setFeatureValue("ExposureAuto", "Continuous"))
            return false;

        int32_t exposure_auto_max = settings.shutter_speed * 1000;
        if (!setFeatureValue("ExposureAutoMax", exposure_auto_max))
            return false;


        int32_t exposure_auto_min = settings.shutter_speed * 1000 * 0.3;
        if (!setFeatureValue("ExposureAutoMin", exposure_auto_min))
            return false;

        if (!setFeatureValue("ExposureAutoTarget", settings.brightness))
            return false;
        if (!setFeatureValue("ExposureAutoAlg", "FitRange"))
            return false;
        if (!setFeatureValue("ExposureAutoRate", 30))
            return false;
    }
    else
    {
        //manual/fixed exposure
        if (featureExists("ExposureTimeAbs")) {
            if (!setFeatureValue("ExposureTimeAbs", settings.shutter_speed * 1000))
                return false;
            if (!setFeatureValue("ExposureAuto", "Off"))
                return false;
        } else {
            if (!setFeatureValue("ExposureTime", settings.shutter_speed * 1000))
                return false;
            if (!setFeatureValue("ExposureAuto", "Off"))
                return false;
        }
    }
    if (featureExists("ExposureMode")) {
        if (!setFeatureValue("ExposureMode", "Timed"))
            return false;
    }

    std::string trigger_delay_feature = "TriggerDelayAbs";

    if (!featureExists(trigger_delay_feature))
        trigger_delay_feature = "TriggerDelay";

    if (!setFeatureValue(trigger_delay_feature, (float)settings.hw_trigger_delay_us))
        return false;

    setupGPIO();

    switch (settings.acquisition_mode)
    {
        case ACQUISITION_CONTINUOUS:
        case ACQUISITION_HW_TRIGGER:
        case ACQUISITION_SW_TRIGGER:
            if (!setFeatureValue("AcquisitionMode", "Continuous"))
                return false;
            break;
        case ACQUISITION_RECORDER_MODE:
            if (!setFeatureValue("AcquisitionMode", "Recorder"))
                return false;
            break;
        default:
            break;
    }

    if (camera->GetFeatureByName("TriggerSelector", feature) != VmbErrorSuccess)
    {
            log->error("GetFeatureByName(TriggerSelector) failed");
            return false;
    }
    bool writeable, valueavailable;
    feature->IsWritable(writeable);
    feature->IsValueAvailable("AcquisitionRecord", valueavailable);
    log->info("Trigger selector, writeable: {}, AcquisitionRecord selectable: {}", writeable, valueavailable);

    switch (settings.acquisition_mode)
    {
        case ACQUISITION_CONTINUOUS:
        case ACQUISITION_HW_TRIGGER:
        case ACQUISITION_SW_TRIGGER:
            if (feature->SetValue("FrameStart")) {
                  log->error("TriggerSelector = FrameStart failed");
                 return false;
            }
        break;
        case ACQUISITION_RECORDER_MODE:
            if (feature->SetValue("AcquisitionRecord") != VmbErrorSuccess) {
                log->error("TriggerSelector = AcquisitionRecord failed");
                return false;
            }
            log->info("TriggerSelector = AcquisitionRecord");
            break;
        default:
        break;
    }

    std::string trigger_source;
    switch (settings.acquisition_mode)
    {
        case ACQUISITION_CONTINUOUS:
            if (featureExists("TriggerMode")) {
                trigger_source = "Software";
            } else {
                trigger_source = "Freerun";
            }
            log->info("Starting freerun capture!");
        break;
    case ACQUISITION_HW_TRIGGER:
    case ACQUISITION_RECORDER_MODE:
            trigger_source = "Line";
            if (_outputControlType == OUTPUT_CONTROL_MAKO)
                trigger_source = "Line1";
            else if (_outputControlType == OUTPUT_CONTROL_ALVIUM)
                trigger_source = "Line3";
            else {
                log->error("invalid outputControlType={}", (int)(_outputControlType));
                return false;
            }
            log->info("Enabling hardware trigger ({})", trigger_source);
        break;
    case ACQUISITION_SW_TRIGGER:
            trigger_source = "Software";
            log->info("Enabling software trigger!");
        break;
    default:
        break;
    }
    if (!setFeatureValue("TriggerSource", trigger_source.c_str()))
        return false;
    if (featureExists("TriggerActivation")) {
        setFeatureValue("TriggerActivation", "RisingEdge");
    }
    if (featureExists("TriggerMode")) {
        std::string val = "On";
        if (settings.acquisition_mode == ACQUISITION_CONTINUOUS)
            val = "Off";
        if (!setFeatureValue("TriggerMode", val.c_str()))
            return false;
    }

    if (featureExists("AcquisitionFrameCount")) {
        //only exists on Mako
        setFeatureValue("AcquisitionFrameCount", 1);
        setFeatureValue("RecorderPreEventCount", 1);
        setFeatureValue("StreamBytesPerSecond", 100 * 1024 * 1024);
    }

    if (featureExists("AcquisitionFrameRateEnable")) {
        setFeatureValue("AcquisitionFrameRateEnable", true);
        setFeatureValue("AcquisitionFrameRate", settings.fps);
        log->info("set fps to {}", settings.fps);
    }

    std::string gain_auto = settings.gain_mode == GAIN_AUTO ? "Continuous" : "Off";
    if (featureExists("GainAuto")) {
        if (!setFeatureValue("GainAuto", gain_auto.c_str()))
            return false;
        if (!setFeatureValue("Gain", settings.gain_fixed))
            return false;
    }

    _is_opened = startCapture();
    log->info("_is_opened={}", _is_opened);
    return _is_opened;
}

bool VimbaCapture::trigger()
{
    if (!_is_opened) {
        return false;
    }
    return execFeatureCommand("TriggerSoftware");
}


bool VimbaCapture::capture(Mat &image)
{
    FramePtr frame;

    auto timer = PerfTimer();
    VmbUchar_t *data;

    timer.start();
    VmbErrorType ret = camera->AcquireSingleImage(frame, 1000);

    if (ret != VmbErrorSuccess)
    {
        log->error("AcquireSingleImage failed");
        return false;
    } else {
        log->info("Acquired frame in {}ms", timer.end_ms());
    }

    ret = frame->GetImage(data);

    if (ret != VmbErrorSuccess)
    {
        log->error("GetImage failed");
        return false;
    }

    Mat output(settings.resolution_y, settings.resolution_x, CV_8U, (uint8_t*)data);
    output.copyTo(image);

    return true;
}

unsigned int  VimbaCapture::getCameraCount() 
{
    CameraPtrVector cameras;
	sys.GetCameras( cameras );

	return cameras.size();
}

bool VimbaCapture::getCameraInfo(unsigned int index, CameraInfo& info)
{
    CameraPtrVector cameras;
	sys.GetCameras(cameras);

    info.camera_index = index;
	if (index < cameras.size())
    {
		cameras[index]->GetID(info.identifier);
		cameras[index]->GetName(info.name);
		cameras[index]->GetModel(info.model);
		cameras[index]->GetSerialNumber(info.serial_number);
        return true;
    }
			
	return false;
}

bool VimbaCapture::getFrame(Mat& frame)
{
    if (!_is_opened)
        return false;

    VmbUchar_t *data;

    FramePtr frame_ptr = frame_observer->GetFrame();
    if (!frame_ptr)
        return false;

    VmbErrorType ret = frame_ptr->GetImage(data);

    if (ret != VmbErrorSuccess)
    {
        log->error("GetImage failed");
        return false;
    }

    Mat output(settings.resolution_y, settings.resolution_x, CV_8U, (uint8_t*)data);
    output.copyTo(frame);
    camera->QueueFrame(frame_ptr);
    return true;
}

bool VimbaCapture::setOutputState(bool state)
{
    if (_outputControlType == OUTPUT_CONTROL_MAKO)
        return setFeatureValue("SyncOutLevels", (int)state);
    else if (_outputControlType == OUTPUT_CONTROL_ALVIUM) {
        return setFeatureValue("LineInverter", state);
    } else
        log->error("invalid output control type");
    return false;
}

bool VimbaCapture::getOutputState(bool &state)
{

    if (_outputControlType == OUTPUT_CONTROL_MAKO)
        return getFeatureValue("SyncOutLevels", state);
    else if (_outputControlType == OUTPUT_CONTROL_ALVIUM) {
        return getFeatureValue("LineStatus", state);
    } else
        log->error("invalid output control type");
    return false;
}

bool VimbaCapture::prepareCamera()
{
    if (!setFeatureValue("UserSetSelector", "Default"))
        return false;

    if (!execFeatureCommand("UserSetLoad"))
        return false;

    if (!execFeatureCommand("GVSPAdjustPacketSize"))
        return false;

    return true;
}

void VimbaCapture::observerFrameReceived(int status)
{
    if (settings.acquisition_mode == ACQUISITION_RECORDER_MODE)
    {
        //stop and start acqusition after every image
        execFeatureCommand("AcquisitionStop");
        execFeatureCommand("AcquisitionStart");
    }


    emit frameReceivedSignal(status);
}

bool VimbaCapture::setupGPIO()
{
    if (featureExists("SyncOutSource")) {
        _outputControlType = OUTPUT_CONTROL_MAKO;
        log->info("output control type=mako");
        //Mako has SyncOutSource
        if (!setFeatureValue("SyncOutSource", "GPO"))
            return false;
        if (!setFeatureValue("SyncOutSelector", "SyncOut1"))
            return false;
    } else {
        _outputControlType = OUTPUT_CONTROL_ALVIUM;
        log->info("output control type=alvium");
        //Alvium has a different way of controlling outputs- essentially the line
        // is configured as output, not driven by anything, so it's always 0. Invert functionality is abused to software-control the output
        if (!setFeatureValue("LineSelector", "Line2"))
            return false;
        if (!setFeatureValue("LineMode", "Output"))
            return false;
        if (!setFeatureValue("LineSource", "Off"))
            return false;
    }

    if (!setOutputState(false))
        return false;

    return true;
}
/**
 * @brief Read sensor height and width, update private variables
 * @return
 */
bool VimbaCapture::readSensorInfo()
{
    if (!getFeatureValue("SensorHeight", m_sensor_height))
        return false;
    if (!getFeatureValue("SensorWidth", m_sensor_width))
        return false;

    log->info("camera SensorWidth={},SensorHeight={}", m_sensor_width, m_sensor_height);

    if (!getFeatureValue("WidthMax", m_sensor_width_max))
        return false;
    if (!getFeatureValue("HeightMax", m_sensor_height_max))
        return false;

    log->info("camera WidthMax={}, HeightMax={}", m_sensor_width_max, m_sensor_height_max);

    uint32_t offset_x, offset_y;
    if (!getFeatureValue("OffsetX", offset_x))
        return false;
    if (!getFeatureValue("OffsetY", offset_y))
        return false;

    log->info("camera OffsetX={}, OffsetY={}", offset_x, offset_y);

    return true;
}

bool VimbaCapture::stopCapture()
{
    if (!execFeatureCommand("AcquisitionStop"))
        return false;

    camera->FlushQueue();
    camera->EndCapture();
    camera->RevokeAllFrames();

    //free frame_ptrs, since the payload size could have changed
    m_frame_ptrs.clear();

    return true;
}

bool VimbaCapture::startCapture(void)
{
    FeaturePtr feature;
    VmbErrorType ret;

    //find out payload size for current settings
    uint64_t tmp;
    if (!getFeatureValue("PayloadSize", tmp))
        return false;
    log->info("PayloadSize={}", tmp);
    //allocate buffers, announce them to camera
    for (unsigned int i = 0; i < 5; i++)
    {
        FramePtr ptr(new Frame(tmp));
        m_frame_ptrs.push_back(ptr);

        ret = ptr->RegisterObserver(frame_observer_ptr);
        if (ret != VmbErrorSuccess)
        {
            log->error("RegisterObserver failed");
            return false;
        }

        ret = camera->AnnounceFrame(ptr);
        if (ret != VmbErrorSuccess)
        {
            log->error("AnnounceFrame failed");
            return false;
        }
    }

    //start capture
    ret = camera->StartCapture();
    if (ret != VmbErrorSuccess)
    {
        log->error("StartCapture failed, error code={}", ret);
        return false;
    }

    for (FramePtrVector :: iterator iter = m_frame_ptrs.begin(); m_frame_ptrs.end () != iter; ++iter)
    {
        ret = camera->QueueFrame(*iter);
        if (ret != VmbErrorSuccess)
        {
            log->error("QueueFrame failed, error code={}", ret);
            return false;
        }
    }

    return execFeatureCommand("AcquisitionStart");
}

bool VimbaCapture::execFeatureCommand(const std::string &featureName) {
    FeaturePtr feature;

    if (camera->GetFeatureByName(featureName.c_str(), feature) != VmbErrorSuccess) {
        log->error("GetFeatureByName({}) failed", featureName);
        return false;
    }
    auto ret = feature->RunCommand();
    if (ret != VmbErrorSuccess) {
        log->error("Failed to execute command {}, return value={}", featureName, ret);
        return false;
    }
    return true;
}

bool VimbaCapture::featureExists(const std::string &featureName) {
    FeaturePtr feature;

    if (camera->GetFeatureByName(featureName.c_str(), feature) != VmbErrorSuccess) {
        return false;
    }
    return true;
}

bool VimbaCapture::getFeatureValue(const std::string &featureName, bool &value) {
    FeaturePtr feature;

    if (camera->GetFeatureByName(featureName.c_str(), feature) != VmbErrorSuccess) {
        log->error("GetFeatureByName({}) failed", featureName);
        return false;
    }

    auto ret = feature->GetValue(value);
    if (ret != VmbErrorSuccess) {
        log->error("reading {} failed, return value={}", featureName, ret);
        return false;
    }
    return true;
}


template<typename T>
bool VimbaCapture::getFeatureValue(const std::string &featureName, T &value) {
    FeaturePtr feature;

    if (camera->GetFeatureByName(featureName.c_str(), feature) != VmbErrorSuccess) {
        log->error("GetFeatureByName({}) failed", featureName);
        return false;
    }

    VmbInt64_t tmpval;
    auto ret = feature->GetValue(tmpval);
    if (ret != VmbErrorSuccess) {
        log->error("reading {} failed, return value={}", featureName, ret);
        return false;
    }
    value = (T)tmpval;
    return true;
}

template<typename T>
bool VimbaCapture::setFeatureValue(const std::string &featureName, const T &value) {
    FeaturePtr feature;

    if (camera->GetFeatureByName(featureName.c_str(), feature) != VmbErrorSuccess) {
        log->error("GetFeatureByName({}) failed", featureName);
        return false;
    }
    auto ret = feature->SetValue(value);
    if (ret != VmbErrorSuccess) {
        log->error("setting {}={} failed, return value={}", featureName, value, ret);
        return false;
    }
    return true;
}
