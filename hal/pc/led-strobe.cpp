#include "hal/pc/led-strobe.h"
#include <stdio.h>
#include <math.h>

LEDStrobePc::LEDStrobePc(int enable_gpio_pin, int dim_gpio_pin) : 
    LEDStrobe(),
    _enable_gpio_pin(enable_gpio_pin),
    _dim_gpio_pin(dim_gpio_pin)
{

}

bool LEDStrobePc::initialize(void)
{
    _log->info("initialized, enable GPIO pin = {}, dim GPIO pin = {}", _enable_gpio_pin, _dim_gpio_pin); 
    return true;
}

bool LEDStrobePc::enable(void)
{
    _log->debug("strobe enabled");
    return true;
}

bool LEDStrobePc::disable(void)
{
    _log->debug("strobe disabled");
    return true;
}

bool LEDStrobePc::set_brightness(float brightness)
{
    if (brightness > 100.0f) {
        _log->error("brightness value {} out of bounds", brightness);
        return false;
    }
    if (brightness < 0.0f) {
        _log->error("brightness value {} out of bounds", brightness);
        return false;
    }

    _brightness = brightness;
    
    _log->debug("setting brightness to {}/100", _brightness);

    return true;
}
