#ifndef VIMBACAPTURE_H
#define VIMBACAPTURE_H

#include "image_capture.h"

#include <VimbaCPP/Include/VimbaCPP.h>
#include <queue>
#include "vimba_frame_observer.h"
#include <spdlog/logger.h>

class VimbaCapture : public ImageCaptureInterface
{
    Q_OBJECT
    Q_INTERFACES(ImageCaptureInterface)

public:
    VimbaCapture();
    ~VimbaCapture() override;
    bool init(const image_capture_settings_t &parameters) override;
    bool capture(Mat &image) override;
    unsigned int  getCameraCount() override;
    bool getCameraInfo(unsigned int index, CameraInfo& info) override;
    bool updateSettings(const image_capture_settings_t &parameters) override;
    bool trigger() override;
    bool getFrame(Mat& frame) override;
    bool setOutputState(bool state) override;
    bool getOutputState(bool& state) override;
    bool prepareCamera();

private slots:
    void observerFrameReceived(int status);


private:
    typedef enum {
        OUTPUT_CONTROL_MAKO=0,
        OUTPUT_CONTROL_ALVIUM=1
    } output_control_t;
    output_control_t _outputControlType;
    image_capture_settings_t settings;
    AVT::VmbAPI::VimbaSystem& sys;
    AVT::VmbAPI::CameraPtr camera;
    VimbaFrameObserver* frame_observer;
    AVT::VmbAPI::IFrameObserverPtr frame_observer_ptr;

    AVT::VmbAPI::FramePtrVector m_frame_ptrs;

    unsigned int m_sensor_height;
    unsigned int m_sensor_width;
    unsigned int m_sensor_height_max;
    unsigned int m_sensor_width_max;

    bool setupGPIO(void);
    bool readSensorInfo(void);

    bool stopCapture(void);
    bool startCapture(void);

    template <typename T>   bool setFeatureValue(const std::string& featureName, const T& value);
    template <typename T>   bool getFeatureValue(const std::string& featureName, T& value);
                            bool getFeatureValue(const std::string& featureName, bool& value);

    bool execFeatureCommand(const std::string& feature);

    bool featureExists(const std::string& featureName);
    std::shared_ptr<spdlog::logger> log;
};

#endif // VIMBACAPTURE_H
