#pragma once

#include <spdlog/spdlog.h>
#include "hal/base/led-strobe.h"

class LEDStrobePi : public LEDStrobe {
    public:
        LEDStrobePi(int enable_gpio_pin = 16, int dim_gpio_pin = 26);
        bool initialize(void) override;
        bool enable(void) override;
        bool disable(void) override;
        bool set_brightness(float brightness) override;
    protected:
        int _enable_gpio_pin;
        int _dim_gpio_pin;
        float _brightness;
};
