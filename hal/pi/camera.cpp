#include "hal/pi/camera.h"


CameraPi::CameraPi() :
    Camera()
{

}

bool CameraPi::setup(unsigned int width, unsigned int height)
{
    _camera.setFormat(raspicam::RASPICAM_FORMAT_BGR);
    _camera.setWidth(width);
    _width = width;
    _camera.setHeight(height);
    _height = height;
    _camera.setHorizontalFlip(false);
    _camera.setVerticalFlip(false);

    _camera.setBrightness(40);
    _camera.setShutterSpeed(5000);
    _tmpFrame = cv::Mat(height, width, CV_8UC3);
    _log->info("setup done, w={}, h={}", width, height);
    return true;
}

bool CameraPi::open()
{
    bool ret = _camera.open();
    _log->info("raspicam open result={}", ret);
    return ret;
}

bool CameraPi::captureImage(cv::Mat& mat)
{
    _camera.grab();
    _camera.retrieve(mat.ptr<uchar>(0));
    return true;
}
