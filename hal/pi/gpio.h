#pragma once

#include "hal/base/gpio.h"

namespace omcam {
namespace hal {

class GPIOPi : public GPIO {
    public:
        GPIOPi();
        bool initialize(void) override;
        bool set_output_state(output_t output, io_state_t state) override;
        io_state_t get_input_state(input_t input) override;

    protected:
        int _input_pin_number[IN_END];
        int _output_pin_number[OUT_END];
};

}} //namespace omcam::hal
