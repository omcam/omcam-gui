
/* 
* This file is part of VL53L1 Platform 
* 
*/


#include "vl53l1_platform.h"
#include "vl53l1_error_codes.h"
#include <string.h>
#include <time.h>
#include <math.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <time.h>
#include <linux/i2c-dev.h>
#include <unistd.h>

#include <pigpio.h>


static VL53L1_Dev_t static_dev;
static uint8_t i2c_tx_buf[256];
int VL53L1X_i2c_init(const char * devPath, int devAddr)
{
    int file = i2cOpen(1, devAddr, 0);
    if (file >= 0)
        static_dev.fd = file;

    return file;
}

static int i2c_write(int fd, uint16_t cmd, uint8_t * data, uint8_t datalen){
    uint8_t transaction_len = datalen + 2;

    if (transaction_len > sizeof(i2c_tx_buf))
    {
        printf("too long transaction\n");
        return VL53L1_ERROR_BUFFER_TOO_SMALL;
    }
    i2c_tx_buf[0] = cmd >> 8;
    i2c_tx_buf[1] = cmd & 0xff;
    memcpy(i2c_tx_buf+2, data, datalen);
    int ret = i2cWriteDevice(fd, i2c_tx_buf, transaction_len);
    if (ret != 0)
    {
        printf("i2cWriteDevice failed\n");
        return VL53L1_ERROR_CONTROL_INTERFACE;
    }

    return VL53L1_ERROR_NONE;
}

static int i2c_read(int fd, uint16_t cmd, uint8_t * data, uint8_t len){

    uint8_t buf[2];
    buf[0] = cmd >> 8;
    buf[1] = cmd & 0xff;

    if (i2cWriteDevice(fd, buf, 2) != 0)
    {
        printf("i2cWriteDevice failed\n");
        return VL53L1_ERROR_CONTROL_INTERFACE;
    }
    if (i2cReadDevice(fd, data, len) != len)
    {
        printf("i2cReadDevice failed\n");
        return VL53L1_ERROR_CONTROL_INTERFACE;
    }

    return VL53L1_ERROR_NONE;
}

int8_t VL53L1_WriteMulti( uint16_t dev, uint16_t index, uint8_t *pdata, uint32_t count) {
    return i2c_write(static_dev.fd, index, pdata, count);
}

int8_t VL53L1_ReadMulti(uint16_t dev, uint16_t index, uint8_t *pdata, uint32_t count){
    return i2c_read(static_dev.fd, index, pdata, count);
}

int8_t VL53L1_WrByte(uint16_t dev, uint16_t index, uint8_t data) {
	return i2c_write(static_dev.fd, index, &data, 1);
}

int8_t VL53L1_WrWord(uint16_t dev, uint16_t index, uint16_t data) {
     uint8_t buf[4];
    buf[1] = data>>0&0xFF;
    buf[0] = data>>8&0xFF;
    return i2c_write(static_dev.fd, index, buf, 2);
}

int8_t VL53L1_WrDWord(uint16_t dev, uint16_t index, uint32_t data) {
    uint8_t buf[4];
    buf[3] = data>>0&0xFF;
    buf[2] = data>>8&0xFF;
    buf[1] = data>>16&0xFF;
    buf[0] = data>>24&0xFF;
    return i2c_write(static_dev.fd, index, buf, 4);
}

int8_t VL53L1_RdByte(uint16_t dev, uint16_t index, uint8_t *data) {
    uint8_t tmp = 0;
    int ret = i2c_read(static_dev.fd, index, &tmp, 1);
    *data = tmp;
    // printf("%u\n", tmp);
    return ret;
}

int8_t VL53L1_RdWord(uint16_t dev, uint16_t index, uint16_t *data) {
    uint8_t buf[2];
    int ret = i2c_read(static_dev.fd, index, buf, 2);
    uint16_t tmp = 0;
    tmp |= buf[1]<<0;
    tmp |= buf[0]<<8;
    // printf("%u\n", tmp);
    *data = tmp;
    return ret;
}

int8_t VL53L1_RdDWord(uint16_t dev, uint16_t index, uint32_t *data) {
    uint8_t buf[4];
    int ret = i2c_read(static_dev.fd, index, buf, 4);
    uint32_t tmp = 0;
    tmp |= buf[3]<<0;
    tmp |= buf[2]<<8;
    tmp |= buf[1]<<16;
    tmp |= buf[0]<<24;
    *data = tmp;
    // printf("%zu\n", tmp);
    return ret;
}

int8_t VL53L1_WaitMs(uint16_t dev, int32_t wait_ms){
    usleep(wait_ms * 1000);
    return VL53L1_ERROR_NONE;
}
