#pragma once

#include "hal/base/camera.h"
#include <raspicam/raspicam.h>

class CameraPi : public Camera {
    Q_OBJECT
    Q_INTERFACES(Camera)
public:
    CameraPi();

    bool setup(unsigned int width, unsigned int height) override;
    bool open() override;
    
    //synchronous image capture
    bool captureImage(cv::Mat& mat) override;
    bool isOpened() const override {
        return _camera.isOpened();
    }
protected:
    raspicam::RaspiCam _camera;
    cv::Mat _tmpFrame;
};
