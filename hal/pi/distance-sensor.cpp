#include "hal/pi/distance-sensor.h"
#include <unistd.h>
#include "vl53l1_platform.h"
#include "vl53l1_error_codes.h"
#include "VL53L1X_api.h"

DistanceSensorPi::DistanceSensorPi() : 
    DistanceSensor()
{

}

bool DistanceSensorPi::open(uint8_t addr, const char* bus) {
    _log->info("initializing");

    //_dev.I2cDevAddr = addr;
    uint8_t dev = addr;
    int fd = VL53L1X_i2c_init(bus, addr);
    if (fd < 0) {
        _log->error("failed to open i2c bus: {}", bus);
        return false;
    }
    uint8_t byteData = 0;
    int status = VL53L1_RdByte(dev, 0x010F, &byteData);
    
    _log->info("VL53L1X Model_ID: {:#x}", byteData);
    if (byteData != 0xea)
    {
        _log->error("model id does not match 0xEA");
        return false;
    }
    /*
    status = VL53L1_RdByte(dev, 0x32, &byteData);
    _log->info("0x2D = {:#x}", byteData);
    status = VL53L1_WrByte(dev, 0x32, 0xF1);
    _log->info("write 0x2D = {:#x}", 0xF1);
    status = VL53L1_RdByte(dev, 0x32, &byteData);
    _log->info("0x2D = {:#x}", byteData);
    */
    status = VL53L1_RdByte(dev, 0x0110, &byteData);
    _log->info("VL53L1X Module_Type: {:#x}", byteData);
    
    uint8_t sensorState=0;
    uint8_t counter=0;

    while(sensorState==0) {
        status = VL53L1X_BootState(dev, &sensorState);
        usleep(5000);
        counter++;
        if (counter > 100) {
            _log->error("VL53L1X_BootState timeout");
            return false;
        }
    }

    _log->info("VL53L1X booted, initializing");

    status = VL53L1X_SensorInit(dev); // Data initialization
    if (status != VL53L1_ERROR_NONE)
    {
        _log->error("VL53L1X_SensorInit failed");
        return false;
    }

    _log->info("VL53L1X_SensorInit complete, setting distance mode");

    status = VL53L1X_SetDistanceMode(dev, 2); // 1=short, 2=long
    if (status != VL53L1_ERROR_NONE)
    {
        _log->error("SetDistanceMode failed");
        return false;
    }

    _log->info("VL53L1X_SetDistanceMode complete, setting timing budget");
    status = VL53L1X_SetTimingBudgetInMs(dev, 100); // In ms, possible values [20, 50, 100, 200, 500]
    if (status != VL53L1_ERROR_NONE)
    {
        _log->error("SetTimingBudgetInMs failed");
        return false;
    }

    _log->info("VL53L1X_SetTimingBudgetInMs complete, setting timing budget");
    status = VL53L1X_SetInterMeasurementInMs(dev, 100); // In ms
    if (status != VL53L1_ERROR_NONE)
    {
        _log->error("SetInterMeasurementInMs failed");
        return false;
    }

    _log->info("VL53L1X initialized");
    return true;
}

/*
void DistanceSensorPi::print_pal_error(VL53L0X_Error status){
    char buf[VL53L0X_MAX_STRING_LENGTH];
    VL53L0X_GetPalErrorString(status, buf);
    _log->error("API status: {}, {}", status, buf);
}
*/

bool DistanceSensorPi::start_continuous_ranging(void)
{
    int status = VL53L1X_StartRanging(0);
    if (status != VL53L1_ERROR_NONE) {
        _log->error("StartRanging failed");
        return false;
    }

    _ranging_started = true;
    return _ranging_started;
}

bool DistanceSensorPi::stop_continuous_ranging(void)
{
    /*
    VL53L0X_Error status = VL53L0X_StopMeasurement(&_dev);
    if (status != VL53L0X_ERROR_NONE) {
        _log->error("StopMeasurement failed");
        print_pal_error(status);
        return false;
    }
    */
    _ranging_started = false;
    return true;
}
 
bool DistanceSensorPi::wait_measurement(void) {
    int status = VL53L1_ERROR_NONE;
    uint32_t retry_cnt = 50;

    while (retry_cnt > 0) {
        uint8_t data_ready = 0;

        status = VL53L1X_CheckForDataReady(0, &data_ready);
        if (status == VL53L1_ERROR_NONE) {
            if (data_ready == 1)
                return true;
        } else {
            return false;
        }

        retry_cnt--;
        usleep(5000);
    }

    return false;
}

DistanceSensor::range_status_t DistanceSensorPi::read_range(float& range_mm)
{

    uint8_t RangeStatus;
    uint16_t Distance;
    uint16_t SignalRate;
    uint16_t AmbientRate;
    uint16_t SpadNum; 

    DistanceSensor::range_status_t rs = STATUS_ERROR;

    int status = VL53L1X_GetRangeStatus(0, &RangeStatus);
    
    if (status != VL53L1_ERROR_NONE)
    {
        _log->error("GetRangeStatus failed");
        rs = STATUS_ERROR;
        goto out;
    }

    status = VL53L1X_GetDistance(0, &Distance);
    if (status != VL53L1_ERROR_NONE)
    {
        _log->error("GetDistance failed");
        rs = STATUS_ERROR;
        goto out;
    }
     
    if (status == VL53L1_ERROR_NONE)
    {
        if (RangeStatus == 0) {
            range_mm = Distance;
            rs = STATUS_RANGE_VALID;
        } else {
            rs = STATUS_RANGE_INVALID;
        }
    } else {
        rs = STATUS_ERROR;
    }

out:
    status = VL53L1X_ClearInterrupt(0); /* clear interrupt has to be called to enable next interrupt*/

    return rs;
}
