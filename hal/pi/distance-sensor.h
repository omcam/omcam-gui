#pragma once

#include <stdint.h>
#include <stdbool.h>

#include <spdlog/spdlog.h>

#include "hal/base/distance-sensor.h"

//#include "vl53l0x_api.h"
//#include "vl53l0x_platform.h"


class DistanceSensorPi : public DistanceSensor {

public:
    DistanceSensorPi();
    bool open(uint8_t addr, const char* bus);
    
    bool start_continuous_ranging(void);
    bool stop_continuous_ranging(void);
    bool wait_measurement(void);
    bool continuous_ranging_started() { return _ranging_started; }
    DistanceSensor::range_status_t read_range(float& range_mm);

protected:
    //VL53L0X_Dev_t                   _dev;
    bool                            _ranging_started;
    //void print_pal_error(VL53L0X_Error status);
    
};
