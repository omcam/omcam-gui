#include "hal/pi/gpio.h"
#include <pigpio.h>

using namespace omcam::hal;

GPIOPi::GPIOPi() : GPIO() 
{
    _input_pin_number[IN0] = 21;
    _input_pin_number[IN1] = 20;

    _output_pin_number[OUT0] = 6;
    _output_pin_number[OUT1] = 5;
    _output_pin_number[OUT_LED_USR] = 13;
    _output_pin_number[OUT_LED_ERR] = 19;
}

bool GPIOPi::initialize(void)
{
    for (int i=IN0; i < IN_END; i++)
    {
        gpioSetMode(_input_pin_number[i], PI_INPUT);
        _hw_input_state[i] = gpioRead(_input_pin_number[i]) == 1 ? LOW : HIGH;
    }
    for (int i=OUT0; i < OUT_END; i++)
    {
        gpioWrite(_output_pin_number[i], 0);
        _hw_output_state[i] = LOW;
        gpioSetMode(_output_pin_number[i], PI_OUTPUT);
    }
    _log->info("gpiopi initialized");
    return true;
}


bool GPIOPi::set_output_state(output_t output, io_state_t state)
{
    ASSERT_OUTPUT_VALID(output);
    if (_output_is_forced[output]) 
    {
        _log->info("not setting output {} to state: {} because it's being forced");
        return true;
    }

    _log->info("setting output: {}, state: {}", output, state);
    gpioWrite(_output_pin_number[output], state == LOW ? 0 : 1);
    _hw_output_state[output] = state;
    return true;
}


io_state_t GPIOPi::get_input_state(input_t input)
{
    ASSERT_INPUT_VALID(input);
    if (_input_is_forced[input]) {
        _log->debug("returning forced input state for {}", input);
        return _forced_input_state[input];
    }
    _hw_input_state[input] = gpioRead(_input_pin_number[input]) == 1 ? LOW : HIGH;
    return _hw_input_state[input];
}
