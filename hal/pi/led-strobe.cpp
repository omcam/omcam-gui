#include "led-strobe.h"
#include <stdio.h>
#include <math.h>
#include <pigpio.h>

static const int PWM_FREQUENCY=5000;

LEDStrobePi::LEDStrobePi(int enable_gpio_pin, int dim_gpio_pin) : 
    LEDStrobe(),
    _enable_gpio_pin(enable_gpio_pin),
    _dim_gpio_pin(dim_gpio_pin)
{

}

bool LEDStrobePi::initialize(void)
{
    int ret = gpioInitialise();
    if (ret < 0) {
        _log->error("gpioInitialise() failed: {}", ret);
    }

    gpioWrite(_enable_gpio_pin, 0); 
    _enabled = false;
    gpioSetPWMfrequency(_dim_gpio_pin, PWM_FREQUENCY);
    gpioSetPWMrange(_dim_gpio_pin, PWM_FREQUENCY);
    gpioPWM(_dim_gpio_pin, 0);

    gpioSetMode(_enable_gpio_pin, PI_OUTPUT);
    gpioSetMode(_dim_gpio_pin, PI_OUTPUT);

    _log->info("initialized, enable GPIO pin = {}, dim GPIO pin = {}", _enable_gpio_pin, _dim_gpio_pin); 
    return true;
}

bool LEDStrobePi::enable(void)
{
    gpioWrite(_enable_gpio_pin, 1);
    _enabled = true;
    _log->debug("strobe enabled");
    return true;
}

bool LEDStrobePi::disable(void)
{
    gpioWrite(_enable_gpio_pin, 0);
    _enabled = false;
    _log->debug("strobe disabled");
    return true;
}

bool LEDStrobePi::set_brightness(float brightness)
{
    if (brightness > 100.0f) {
        _log->error("brightness value {} out of bounds", brightness);
        return false;
    }
    if (brightness < 0.0f) {
        _log->error("brightness value {} out of bounds", brightness);
        return false;
    }

    _brightness = brightness;
    
    gpioPWM(_dim_gpio_pin, roundf(PWM_FREQUENCY * (_brightness / 100.0f)));

    _log->debug("setting brightness to {}/100", _brightness);

    return true;
}
