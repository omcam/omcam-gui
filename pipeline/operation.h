#pragma once

#include "results.h"
#include "parameters.h" 
#include "perfcounter.h"
#include <spdlog/spdlog.h>

#include <opencv2/core.hpp>
#include <utility>
#include <string>

#include <memory>

namespace omcam {
namespace img {

    class OperationBase {
    public:
        OperationBase(const std::string& name);
        virtual ~OperationBase() {}

        virtual void preExecute(void);
        virtual void execute(const std::shared_ptr<cv::Mat>& input_img,
                std::shared_ptr<cv::Mat>& output_img) = 0;
        virtual void postExecute(void);

        std::string getName();
        void addDebugImage(const std::string&, const std::shared_ptr<cv::Mat>&) {}

        std::string getPerformanceStatistics(void);
    protected:
        std::string _name;
        PerfCounter _perf;
        std::shared_ptr<spdlog::logger> _log;
    };

    template <typename Parameters_type, typename Results_type> class Operation : public OperationBase {
        public:
            Operation(const std::string& name) : OperationBase(name) {}
            Operation(std::string name, const Parameters_type& params) : OperationBase(name),
                _params(params) {}
            virtual ~Operation() {}
            void setParameters(const Parameters_type& params) { _params = params;}

            virtual Results_type& getResults(void) { return _results; }

        protected:
            Parameters_type             _params;
            Results_type                _results;
    };
}
}
