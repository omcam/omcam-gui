#pragma once

#include "operation.h"
#include <utility>
#include <queue>

namespace omcam {
namespace img {

    class Pipeline {
        public:
            Pipeline(const std::string& name) : _name(name) {}
            virtual ~Pipeline() {}

            void execute(
                    const std::shared_ptr<cv::Mat>& input,
                    std::shared_ptr<cv::Mat>& output);

            void appendOperation(const std::shared_ptr<OperationBase>& operation);
            std::string getName(void) { return _name; }
        protected:
            std::vector<std::shared_ptr<OperationBase> > _operations;
            std::string _name;
    };
}
}
