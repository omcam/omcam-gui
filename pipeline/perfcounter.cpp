#include "perfcounter.h"

#include <algorithm>
#include <numeric>
#include <sstream>


using namespace omcam;

PerfCounter::PerfCounter() :
    avg_us(0),
    min_us(UINT32_MAX),
    max_us(),
    total_operations(0)
{
}

void PerfCounter::start()
{
    _t1 = std::chrono::high_resolution_clock::now();
}

uint32_t PerfCounter::stop()
{
    _t2 = std::chrono::high_resolution_clock::now();
    uint32_t delta = std::chrono::duration_cast<std::chrono::microseconds>(_t2 - _t1).count();
    
    min_us = std::min(min_us, delta);
    max_us = std::max(max_us, delta);

    _deltas.push_back(delta);
    if (_deltas.size() > NUM_AVERAGE)
        _deltas.pop_front();

    return delta;
}

void PerfCounter::updateStatistics(void)
{
    if (_deltas.size() > 0)
    {
        uint64_t sum = 0;
        for (auto& elem: _deltas) {
            sum += elem;
        }
        avg_us = sum / _deltas.size();
    }
}

std::string PerfCounter::getStatistics() {
    updateStatistics();
    std::ostringstream sstream;
    sstream << "min=" << min_us <<"us avg=" << avg_us << "us max=" << max_us << "us";
    return sstream.str();
}
