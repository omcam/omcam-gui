#include "pipeline.h"

using namespace omcam::img;

void Pipeline::execute(
                    const std::shared_ptr<cv::Mat>& input,
                    std::shared_ptr<cv::Mat>& output)
{
    std::shared_ptr<cv::Mat> inp(input);
    auto out = std::make_shared<cv::Mat>();

    for (auto &operation: _operations)
    {
        operation->preExecute();
        operation->execute(inp, out);
        operation->postExecute();

        inp = out;
    }
    output = out;
}

void Pipeline::appendOperation(const std::shared_ptr<OperationBase>& operation)
{
    _operations.push_back(operation);
}


