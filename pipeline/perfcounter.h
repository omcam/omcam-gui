#pragma once

#include <opencv2/core.hpp>
#include <queue>
#include <chrono>
#include <string>

#include <stdint.h>

namespace omcam {
    class PerfCounter {
        public:
            PerfCounter();

            uint32_t avg_us;
            uint32_t min_us;
            uint32_t max_us;
            uint32_t total_operations;
            
            void start();
            uint32_t stop();

            void        updateStatistics(void);
            std::string getStatistics();
        protected:
            std::deque<uint32_t> _deltas;

            std::chrono::high_resolution_clock::time_point _t1;
            std::chrono::high_resolution_clock::time_point _t2;
            static const unsigned int NUM_AVERAGE=20;

    };
}
