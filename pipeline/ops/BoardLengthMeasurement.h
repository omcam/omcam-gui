//
// Created by reinis on 02.03.21.
//

#ifndef OMCAM_ACQUISITION_BOARDLENGTHMEASUREMENT_H
#define OMCAM_ACQUISITION_BOARDLENGTHMEASUREMENT_H

#include "../operation.h"
#include "../parameters.h"
#include <stdbool.h>
#include "BoardLengthMeasurementBase.h"

namespace omcam{
    namespace img {

        class BoardLengthMeasurement : public Operation<board_length_measurement_parameters_t ,board_length_measurement_results_t>

        {
        public:
            BoardLengthMeasurement(const std::string& name, const board_length_measurement_parameters_t& params) :
                Operation<board_length_measurement_parameters_t, board_length_measurement_results_t>(name, params)
            {

            }
            void execute(const std::shared_ptr<cv::Mat> &input_img, std::shared_ptr<cv::Mat> &output_img) override;
        protected:

            bool measureBoardFromContour(const cv::Mat& img,
                                         const std::vector<cv::Point>& contour,
                                         board_size_t& outputBoardSize);

            board_size_t boardCornersToSize(std::vector<cv::Point2f> corners);
        };


    }
}

#endif //OMCAM_ACQUISITION_BOARDLENGTHMEASUREMENT_H
