//
// Created by reinis on 03.03.21.
//

#include "Rectification.h"
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>

void
omcam::img::Rectification::execute(const std::shared_ptr<cv::Mat> &input_img, std::shared_ptr<cv::Mat> &output_img) {
    //cv::undistort(*input_img, *output_img, _cameraMatrix, _distortionCoefficients);
    cv::remap(*input_img, *output_img, _map1, _map2, cv::INTER_LINEAR);
}

bool omcam::img::Rectification::loadParameters(const std::string &filename) {
    cv::FileStorage param_fs(filename, cv::FileStorage::READ);
    if (!param_fs.isOpened())
    {
        return false;
    }

    param_fs["camera_matrix"] >> _cameraMatrix;
    param_fs["distortion_coefficients"] >> _distortionCoefficients;
    std::string calibration_time;
    param_fs["calibration_time"] >> calibration_time;

    param_fs["image_width"] >> _inputImageSize.width;
    param_fs["image_height"] >> _inputImageSize.height;

    //additional image size
    double add_px_x = 100, add_px_y;
    _calibratedImageSize = _inputImageSize;
    _calibratedImageSize.width += 2 * add_px_x;
    _calibratedImageSize.height += 2 * add_px_y;

    cv::Mat newCameraMat = _cameraMatrix.clone();
    newCameraMat.at<double>(0, 2) += add_px_x;
    newCameraMat.at<double>(1, 2) += add_px_y;
    cv::initUndistortRectifyMap(_cameraMatrix, _distortionCoefficients,
                                cv::Mat(), newCameraMat, _calibratedImageSize, CV_16SC2, _map1, _map2);

    _parametersLoaded = true;
    return true;
}

omcam::img::Rectification::Rectification(const std::string &name, const std::string &coefficients_filename) :
        Operation<std::string, rectification_results_t>(name, coefficients_filename),
        _parametersLoaded(false)
{
    if (!_params.empty())
        loadParameters(_params);
}
