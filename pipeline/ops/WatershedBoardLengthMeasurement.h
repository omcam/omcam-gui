//
// Created by reinis on 19.04.21.
//

#ifndef OMCAM_ACQUISITION_WATERSHEDBOARDLENGTHMEASUREMENT_H
#define OMCAM_ACQUISITION_WATERSHEDBOARDLENGTHMEASUREMENT_H

#include "../operation.h"
#include "../pipeline.h"
#include "BoardLengthMeasurementBase.h"

namespace omcam {
    namespace img {

        class WatershedBoardLengthMeasurement : public Operation<board_length_measurement_parameters_t ,board_length_measurement_results_t>
        {
        public:
            WatershedBoardLengthMeasurement(const std::string& name, const board_length_measurement_parameters_t& params) :
                    Operation<board_length_measurement_parameters_t, board_length_measurement_results_t>(name, params)
            {

            }

            void execute(const std::shared_ptr<cv::Mat> &input_img, std::shared_ptr<cv::Mat> &output_img) override;

        protected:
            void laplacianFilter(cv::Mat& input, cv::Mat& output);
            void distanceTransform(cv::Mat& input, cv::Mat& output);
            void prepareWatershedMarkers(cv::Mat& input, cv::Mat& output);

            bool measureBoardFromContour(const cv::Mat &img,
                                    const std::vector<cv::Point> &contour,
                                    omcam::img::board_size_t &outputBoardSize,
                                    std::vector<cv::Point2f>& corners);
            std::vector<cv::Point2f> refineCorners(const cv::Mat& img, const std::vector<cv::Point2f>& corners);
            board_size_t boardCornersToSize(std::vector<cv::Point2f> corners);
        };

    }
}


#endif //OMCAM_ACQUISITION_WATERSHEDBOARDLENGTHMEASUREMENT_H
