//
// Created by reinis on 03.03.21.
//

#ifndef OMCAM_ACQUISITION_RECTIFICATION_H
#define OMCAM_ACQUISITION_RECTIFICATION_H

#include "../operation.h"

namespace omcam {
    namespace img {

        typedef struct {
        } rectification_results_t;

    class Rectification : public Operation<std::string,rectification_results_t> {
    public:
        Rectification(const std::string& name, const std::string& coefficients_filename);
        void execute(const std::shared_ptr<cv::Mat> &input_img, std::shared_ptr<cv::Mat> &output_img) override;
        bool loadParameters(const std::string& filename);
        bool parametersLoaded() const { return _parametersLoaded; }
    protected:
        cv::Mat _map1, _map2;
        cv::Size _inputImageSize;
        cv::Size _calibratedImageSize;

        cv::Mat _cameraMatrix;
        cv::Mat _distortionCoefficients;
        bool _parametersLoaded;
        std::shared_ptr<spdlog::logger> _log;

    };

    }
}


#endif //OMCAM_ACQUISITION_RECTIFICATION_H
