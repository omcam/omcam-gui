//
// Created by reinis on 19.04.21.
//

#ifndef OMCAM_ACQUISITION_BOARDLENGTHMEASUREMENTBASE_H
#define OMCAM_ACQUISITION_BOARDLENGTHMEASUREMENTBASE_H

#include <vector>
#include <stdint.h>
#include <opencv2/core.hpp>

namespace omcam {
    namespace img {
        typedef struct {
            uint8_t threshold_min;
            cv::Rect2d roi;
            float sf_length_poly_a0;
            float sf_length_poly_a1;
            float sf_length_poly_a2;
            float sf_length_poly_a3;

            float sf_width_poly_a0;
            float sf_width_poly_a1;
            float sf_width_poly_a2;
            float sf_width_poly_a3;
        } board_length_measurement_parameters_t;
        typedef struct {
            float board_length_mm;
            float board_length_px;
            float board_width_mm;
            float board_width_px;
            std::vector <cv::Point2f> corners;
        } board_size_t;

        typedef struct {
            bool ok;
            std::vector <board_size_t> boards;
            cv::Mat corners_dbg_mat;
        } board_length_measurement_results_t;
    }
}

#endif //OMCAM_ACQUISITION_BOARDLENGTHMEASUREMENTBASE_H
