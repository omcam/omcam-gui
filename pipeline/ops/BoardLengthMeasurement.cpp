//
// Created by reinis on 02.03.21.
//

#include "BoardLengthMeasurement.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

void omcam::img::BoardLengthMeasurement::execute(const std::shared_ptr<cv::Mat> &input_img,
                                                 std::shared_ptr<cv::Mat> &output_img) {

    cv::Mat threshold_img;
    cv::Mat input_roi = input_img->operator()(_params.roi);

    cv::Mat channels[3];
    cv::Mat input_g;
    if (input_roi.type() != CV_8UC1) {
        //cv::cvtColor(input_roi, input_roi, cv::ColorConversionCodes::COLOR_BGR2HSV);
        cv::split(input_roi, channels);
        input_g = channels[1];
        std::cout << input_g.type() << std::endl;
    } else {
        input_g = input_roi;
    }

    cv::threshold(input_g, threshold_img, _params.threshold_min, 255, cv::THRESH_BINARY);

    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(threshold_img, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    _results.boards.clear();
    input_g.copyTo(_results.corners_dbg_mat);
    for (auto& contour: contours) {
        auto area = cv::contourArea(contour);
        if (area > 10000) {
            board_size_t bs;
            if (measureBoardFromContour(input_g, contour, bs))
                _results.boards.push_back(bs);
            cv::polylines(_results.corners_dbg_mat, contour,
                          true, 0, 2);
        }
    }
    _results.ok = !_results.boards.empty();
}

bool
omcam::img::BoardLengthMeasurement::measureBoardFromContour(const cv::Mat &img, const std::vector<cv::Point> &contour,
                                                            omcam::img::board_size_t &outputBoardSize) {

    /**
     * General idea of the algorithm below: find the best candidates of the corners
     * of input image, then improve the corners using findCornersSubpix
     */
    std::vector<cv::Point> hull;
    cv::convexHull(contour, hull);

    if (hull.size() < 4)
        return false;
    //            left_edge_x                        right_edge_x
    // left_min_y     __________________________________    right_min_y
    //                |                                  |
    //                |                                  |
    // left_max_y      ----------------------------------  right_max_y

    //find the minimum and maximum X coordinates

    int left_edge_x=img.cols, right_edge_x=0;
    for (auto& p: hull) {
        if (p.x < left_edge_x)
            left_edge_x = p.x;
        if (p.x > right_edge_x)
            right_edge_x = p.x;
    }

    //find the minimum and maximum Y coordinates for left and right edges
    int left_min_y = img.rows, left_max_y = 0;
    int right_min_y = img.rows, right_max_y = 0;

    //search window near *_edge_x (to determine if the point in question is near the edges)
    const int WINDOW = 15;
    for (auto&p: hull) {
        if (p.x > (left_edge_x - WINDOW) && p.x < (left_edge_x + WINDOW)) {
            //point is near the left edge
            if (p.y < left_min_y)
                left_min_y = p.y;
            if (p.y > left_max_y)
                left_max_y = p.y;
        }
        if (p.x > (right_edge_x - WINDOW) && p.x < (right_edge_x + WINDOW)) {
            //point is near the right edge
            if (p.y < right_min_y)
                right_min_y = p.y;
            if (p.y > right_max_y)
                right_max_y = p.y;
        }
    }

    cv::Point2f left_top_origin(left_edge_x, left_min_y);
    cv::Point2f left_bottom_origin(left_edge_x, left_max_y);
    cv::Point2f right_top_origin(right_edge_x, right_min_y);
    cv::Point2f right_bottom_origin(right_edge_x, right_max_y);

    cv::Point2f left_top_pt, left_bottom_pt, right_top_pt, right_bottom_pt;
    float left_top_min_dist=img.cols, left_bottom_min_dist=img.cols;
    float right_top_min_dist=img.cols, right_bottom_min_dist=img.cols;

    for (auto& p: hull) {
        if (p.x > (left_edge_x - WINDOW) && p.x < (left_edge_x + WINDOW)) {
            float dx, dy, len;
            dx = p.x - left_top_origin.x;
            dy = p.y - left_top_origin.y;
            len = sqrtf(dx * dx + dy * dy);
            if (len < left_top_min_dist) {
                left_top_min_dist = len;
                left_top_pt = p;
            }
            dx = p.x - left_bottom_origin.x;
            dy = left_bottom_origin.y - p.y;
            len = sqrtf(dx * dx + dy * dy);
            if (len < left_bottom_min_dist) {
                left_bottom_min_dist = len;
                left_bottom_pt = p;
            }
        }

        if (p.x > (right_edge_x - WINDOW) && p.x < (right_edge_x + WINDOW)) {
            float dx, dy, len;
            dx = p.x - right_top_origin.x;
            dy = p.y - right_top_origin.y;
            len = sqrtf(dx * dx + dy * dy);
            if (len < right_top_min_dist) {
                right_top_min_dist = len;
                right_top_pt = p;
            }
            dx = p.x - right_bottom_origin.x;
            dy = right_bottom_origin.y - p.y;
            len = sqrtf(dx * dx + dy * dy);
            if (len < right_bottom_min_dist) {
                right_bottom_min_dist = len;
                right_bottom_pt = p;
            }
        }
    }

    std::vector<cv::Point2f> corners = {
            left_top_pt, left_bottom_pt, right_top_pt, right_bottom_pt};


    auto winSize = cv::Size( WINDOW, WINDOW);
    auto zeroZone = cv::Size( -1, -1 );
    auto criteria = cv::TermCriteria( cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 40, 0.001 );

    cv::cornerSubPix(img, corners, winSize, zeroZone, criteria);
    outputBoardSize = boardCornersToSize(corners);
    for (auto& c: corners) {
        cv::circle(_results.corners_dbg_mat, c, 5, 0, 3);
    }

    return true;
    }

omcam::img::board_size_t omcam::img::BoardLengthMeasurement::boardCornersToSize(std::vector<cv::Point2f> corners) {
    auto& left_top_pt = corners[0], left_bottom_pt = corners[1];
    auto& right_top_pt = corners[2], right_bottom_pt = corners[3];

    cv::Point2f dt = right_top_pt - left_top_pt;
    float top_edge_len = sqrtf(dt.x*dt.x + dt.y*dt.y);
    dt = right_bottom_pt - left_bottom_pt;
    float bottom_edge_len = sqrtf(dt.x*dt.x + dt.y*dt.y);
    board_size_t s;
    s.board_length_mm = s.board_width_mm = s.board_length_px = s.board_width_px = 0.0f;
    s.board_length_px = right_top_pt.x;
    double lp = s.board_length_px;
    s.board_length_mm = _params.sf_length_poly_a0 +
            _params.sf_length_poly_a1 * lp +
            _params.sf_length_poly_a2 * pow(lp, 2.0f) +
            _params.sf_length_poly_a3 * pow(lp, 3.0f);


    dt = left_bottom_pt - left_top_pt;
    float left_edge_len = sqrtf(dt.x*dt.x + dt.y*dt.y);

    dt = right_bottom_pt - right_top_pt;
    float right_edge_len = sqrtf(dt.x*dt.x + dt.y*dt.y);
    s.board_width_px = 0;//(left_edge_len + right_edge_len) / 2.0f;
    s.board_width_mm = 0;//s.board_width_px * _params.sf_width_poly_a1 + _params.sf_width_poly_a0;
    s.corners = corners;

    return s;
}
