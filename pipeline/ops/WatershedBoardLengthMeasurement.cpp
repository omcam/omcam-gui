//
// Created by reinis on 19.04.21.
//

#include "WatershedBoardLengthMeasurement.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

static void dbgimg(const char* title, const cv::Mat& img) {
#ifndef NDEBUG
    //cv::imshow(title, img);
#endif
}

void omcam::img::WatershedBoardLengthMeasurement::execute(
        const std::shared_ptr<cv::Mat> &input_img,
        std::shared_ptr<cv::Mat> &output_img)
{
    cv::Mat input_laplacian;
    cv::Mat input_roi = input_img->operator()(_params.roi);

    cv::Mat input_roi_cvt;
    //code below expects BGR images, provide it to them
    if (input_roi.type() == CV_8UC1) {
        cv::cvtColor(input_roi, input_roi_cvt, cv::COLOR_GRAY2BGR);
    } else {
        input_roi_cvt = input_roi;
    }

    laplacianFilter(input_roi_cvt, input_laplacian);
    dbgimg("laplacian", input_roi_cvt);

    cv::Mat dist;
    distanceTransform(input_laplacian, dist);
    dbgimg("distance transform", dist);

    cv::Mat markers;
    prepareWatershedMarkers(dist, markers);

    cv::Mat markers8u;
    markers.convertTo(markers8u, CV_8U, 10);
    dbgimg("markers", markers8u);

    cv::watershed(input_laplacian, markers);




    cv::Mat mark;
    markers.convertTo(mark, CV_8U);
    bitwise_not(mark, mark);

    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    auto markers_roi = cv::Rect2i(5, 5, mark.cols - 10, mark.rows - 10);

    mark = mark(markers_roi);
    dbgimg("markersv2", mark);

    cv::findContours(mark, contours, hierarchy, cv::RETR_EXTERNAL,
                     cv::CHAIN_APPROX_SIMPLE);

    _results.boards.clear();

    cv::Mat input_8u;
    cv::cvtColor(input_roi_cvt(markers_roi), input_8u, cv::COLOR_BGR2GRAY);

    input_roi_cvt(markers_roi).copyTo(_results.corners_dbg_mat);

    for (auto& contour: contours) {
        auto area = cv::contourArea(contour);
        auto rect = cv::boundingRect(contour);
        std::cout << "contour area: " << area <<
                  ", bounding rect: x: " << rect.x << ", y: " << rect. y << ", w: " << rect.width << ", h: " << rect.height << std::endl;

        if (rect.x == 0 && area > 1000) //this must be it!
        {
            board_size_t bs;
            std::vector<cv::Point2f> corners;
            if (measureBoardFromContour(input_8u, contour, bs, corners)) {
                int board_center_y = corners[0].y + (corners[1].y - corners[0].y)/2;
                cv::Mat board_center_line = mark(cv::Rect2i(0, board_center_y, mark.cols, 1));

                int x;
                for (x=board_center_line.cols-1; x > 0; x--) {
                    if (board_center_line.at<uint8_t>(0, x) > 150)
                        break;
                }
                cv::line(_results.corners_dbg_mat, cv::Point(0, board_center_y), cv::Point(x, board_center_y), cv::Scalar(0, 0, 255), 3);

                dbgimg("board_center_line", board_center_line);
                if (abs(x- corners[2].x) > 5) {
                    corners[2].x = x;
                    corners[3].x = x;
                    bs = boardCornersToSize(corners);
                    std::cout << "x from board_center_line differs significantly, using x=" << x << std::endl;
                }
                _results.boards.push_back(bs);
            }
            cv::polylines(_results.corners_dbg_mat, contour, true, cv::Scalar(0,0, 255), 1);
        }
   }
    _results.ok = !_results.boards.empty();
}

void omcam::img::WatershedBoardLengthMeasurement::laplacianFilter(
        cv::Mat &input, cv::Mat &output)
{
    cv::Mat kernel = (cv::Mat_<float>(3, 3) <<
            1, 1, 1,
            1, -8, 1,
            1, 1, 1);
    cv::Mat laplacian;
    cv::filter2D(input, laplacian, CV_32F, kernel);
    cv::Mat sharp;
    input.convertTo(sharp, CV_32F);

    output = sharp - laplacian;
    output.convertTo(output, CV_8UC3);
}

void omcam::img::WatershedBoardLengthMeasurement::distanceTransform(
        cv::Mat &input, cv::Mat &output)
{
    cv::Mat bw;
    if (input.type() == CV_8UC3)
        cv::cvtColor(input, bw, cv::COLOR_BGR2GRAY);
    else
        input.copyTo(bw);

    cv::threshold(bw, bw, 40, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
    dbgimg("binary", bw);

    cv::distanceTransform(bw, output, cv::DIST_L2, 5);
    cv::normalize(output, output, 0, 1.0, cv::NORM_MINMAX);
}

void omcam::img::WatershedBoardLengthMeasurement::prepareWatershedMarkers(
        cv::Mat &input, cv::Mat &output)
{
    cv::Mat thr;
    cv::threshold(input, thr, 0.3, 1.0, cv::THRESH_BINARY);
    cv::Mat kernel = cv::Mat::ones(3, 3, CV_8U);
    cv::dilate(thr, thr, kernel);

    cv::Mat dist_8u;
    thr.convertTo(dist_8u, CV_8U);

    std::vector<std::vector<cv::Point> > contours;
    cv::findContours(dist_8u, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
    cv::Mat markers = cv::Mat::zeros(thr.size(), CV_32S);
    for (size_t i = 0; i < contours.size(); i++)
    {
        cv::drawContours(markers, contours, static_cast<int>(i), cv::Scalar(static_cast<int>(i)+1), -1);
    }

    //add two lines to top and bottom of image to mark them as background
    cv::line(markers, cv::Point(0, 5), cv::Point(markers.cols, 5), cv::Scalar(255, 255, 255), 5);
    cv::line(markers, cv::Point(0, markers.rows-5), cv::Point(markers.cols, markers.rows-5), cv::Scalar(255, 255, 255), 5);
    //add line to right edge of the image to mark that as background as well
    cv::line(markers, cv::Point(1280 - 20, 0), cv::Point(1280 - 20, markers.rows), cv::Scalar(255, 255, 255), 5);
    output = markers;
}
bool
omcam::img::WatershedBoardLengthMeasurement::measureBoardFromContour(const cv::Mat &img, const std::vector<cv::Point> &contour,
                                                            omcam::img::board_size_t &outputBoardSize,
                                                            std::vector<cv::Point2f> &corners) {

    /**
     * General idea of the algorithm below: find the best candidates of the corners
     * of input image, then improve the corners using findCornersSubpix
     */
    std::vector<cv::Point> hull;
    cv::convexHull(contour, hull);

    if (hull.size() < 4)
        return false;
    //            left_edge_x                        right_edge_x
    // left_min_y     __________________________________    right_min_y
    //                |                                  |
    //                |                                  |
    // left_max_y      ----------------------------------  right_max_y

    //find the minimum and maximum X coordinates

    int left_edge_x=img.cols, right_edge_x=0;
    for (auto& p: hull) {
        if (p.x < left_edge_x)
            left_edge_x = p.x;
        if (p.x > right_edge_x)
            right_edge_x = p.x;
    }

    //find the minimum and maximum Y coordinates for left and right edges
    int left_min_y = img.rows, left_max_y = 0;
    int right_min_y = img.rows, right_max_y = 0;

    //search window near *_edge_x (to determine if the point in question is near the edges)
    const int WINDOW = 5;
    for (auto&p: hull) {
        if (p.x > (left_edge_x - WINDOW) && p.x < (left_edge_x + WINDOW)) {
            //point is near the left edge
            if (p.y < left_min_y)
                left_min_y = p.y;
            if (p.y > left_max_y)
                left_max_y = p.y;
        }
        if (p.x > (right_edge_x - WINDOW) && p.x < (right_edge_x + WINDOW)) {
            //point is near the right edge
            if (p.y < right_min_y)
                right_min_y = p.y;
            if (p.y > right_max_y)
                right_max_y = p.y;
        }
    }

    cv::Point2f left_top_origin(left_edge_x, left_min_y);
    cv::Point2f left_bottom_origin(left_edge_x, left_max_y);
    cv::Point2f right_top_origin(right_edge_x, right_min_y);
    cv::Point2f right_bottom_origin(right_edge_x, right_max_y);

    cv::Point2f left_top_pt, left_bottom_pt, right_top_pt, right_bottom_pt;
    float left_top_min_dist=img.cols, left_bottom_min_dist=img.cols;
    float right_top_min_dist=img.cols, right_bottom_min_dist=img.cols;

    for (auto& p: hull) {
        if (p.x > (left_edge_x - WINDOW) && p.x < (left_edge_x + WINDOW)) {
            float dx, dy, len;
            dx = p.x - left_top_origin.x;
            dy = p.y - left_top_origin.y;
            len = sqrtf(dx * dx + dy * dy);
            if (len < left_top_min_dist) {
                left_top_min_dist = len;
                left_top_pt = p;
            }
            dx = p.x - left_bottom_origin.x;
            dy = left_bottom_origin.y - p.y;
            len = sqrtf(dx * dx + dy * dy);
            if (len < left_bottom_min_dist) {
                left_bottom_min_dist = len;
                left_bottom_pt = p;
            }
        }

        if (p.x > (right_edge_x - WINDOW) && p.x < (right_edge_x + WINDOW)) {
            float dx, dy, len;
            dx = p.x - right_top_origin.x;
            dy = p.y - right_top_origin.y;
            len = sqrtf(dx * dx + dy * dy);
            if (len < right_top_min_dist) {
                right_top_min_dist = len;
                right_top_pt = p;
            }
            dx = p.x - right_bottom_origin.x;
            dy = right_bottom_origin.y - p.y;
            len = sqrtf(dx * dx + dy * dy);
            if (len < right_bottom_min_dist) {
                right_bottom_min_dist = len;
                right_bottom_pt = p;
            }
        }
    }
    corners.clear();
    corners = {left_top_pt, left_bottom_pt, right_top_pt, right_bottom_pt};

    /*
    auto contour_corners = corners;
    auto refined_corners = refineCorners(img, corners);
    auto winSize = cv::Size(10 , 10);
    auto zeroZone = cv::Size( -1, -1 );
    auto criteria = cv::TermCriteria( cv::TermCriteria::EPS, 100, 0.001 );

    cv::cornerSubPix(img, corners, winSize, zeroZone, criteria);


    outputBoardSize = boardCornersToSize(corners);
    for (auto& c: contour_corners) {
        cv::circle(_results.corners_dbg_mat, c, 5, cv::Scalar(0, 255, 0), 1);
    }
     */

    outputBoardSize = boardCornersToSize(corners);
    for (auto& c: corners) {
        cv::circle(_results.corners_dbg_mat, c, 5, cv::Scalar(0, 255, 0), 2);
    }

    return true;
}

omcam::img::board_size_t omcam::img::WatershedBoardLengthMeasurement::boardCornersToSize(std::vector<cv::Point2f> corners) {
    auto& left_top_pt = corners[0], left_bottom_pt = corners[1];
    auto& right_top_pt = corners[2], right_bottom_pt = corners[3];

    cv::Point2f dt = right_top_pt - left_top_pt;
    float top_edge_len = sqrtf(dt.x*dt.x + dt.y*dt.y);
    dt = right_bottom_pt - left_bottom_pt;
    float bottom_edge_len = sqrtf(dt.x*dt.x + dt.y*dt.y);
    board_size_t s;
    s.board_length_mm = s.board_width_mm = s.board_length_px = s.board_width_px = 0.0f;
    s.board_length_px = right_top_pt.x;
    double lp = s.board_length_px;
    s.board_length_mm = _params.sf_length_poly_a0 +
                        _params.sf_length_poly_a1 * lp +
                        _params.sf_length_poly_a2 * pow(lp, 2.0f) +
                        _params.sf_length_poly_a3 * pow(lp, 3.0f);


    dt = left_bottom_pt - left_top_pt;
    float left_edge_len = sqrtf(dt.x*dt.x + dt.y*dt.y);

    dt = right_bottom_pt - right_top_pt;
    float right_edge_len = sqrtf(dt.x*dt.x + dt.y*dt.y);
    s.board_width_px = left_edge_len;
    double wp = s.board_width_px;
    s.board_width_mm = _params.sf_width_poly_a0 +
            _params.sf_width_poly_a1 * wp +
            _params.sf_width_poly_a2 * pow(wp, 2.0f) +
            _params.sf_width_poly_a3 * pow(wp, 3.0f);

    s.corners = corners;

    return s;
}

std::vector<cv::Point2f> omcam::img::WatershedBoardLengthMeasurement::refineCorners(
        const cv::Mat &img,
        const std::vector<cv::Point2f> &corners)
{
    auto left_top_pt = corners[0], left_bottom_pt = corners[1];
    auto right_top_pt = corners[2], right_bottom_pt = corners[3];

    cv::Rect roi = cv::Rect(right_top_pt.x - 10, right_top_pt.y - 10, 20, (right_bottom_pt.y - right_top_pt.y) + 30);
    if (roi.y < 0) {
        roi.y = 0;
    }
    if (roi.x < 0) {
        roi.x = 0;
    }
    if (roi.x + roi.width > img.cols) {
        roi.width = img.cols - roi.x;
    }
    if (roi.y + roi.height > img.rows) {
        roi.height = img.rows - roi.y;
    }
    auto img_roi = img(roi);

    cv::Mat markers = cv::Mat::zeros(img_roi.size(), CV_32S);
    cv::rectangle(markers, cv::Point(0, roi.height / 2 - 5), cv::Point(10, roi.height/2+5), 1, -1);

    cv::line(markers, cv::Point(0, 0), cv::Point(img_roi.cols - 3, 3), 255, 3);
    cv::line(markers, cv::Point(0, img_roi.rows - 3), cv::Point(img_roi.cols - 3, img_roi.rows - 3), 255, 3);
    cv::line(markers, cv::Point(img_roi.cols-3, 0), cv::Point(img_roi.cols - 3, img_roi.rows - 3), 255, 3);

    cv::Mat markers8;
    markers.convertTo(markers8, CV_8U, 10);
    dbgimg("markers", markers8);
    dbgimg("corners refine roi", img_roi);
    /*
    cv::watershed(img_roi, markers);
    cv::Mat mark;
    markers.convertTo(mark, CV_8U);
    cv::bitwise_not(mark, mark);
    dbgimg("corners roi watershed", mark);
     */
    return std::vector<cv::Point2f>();
}
