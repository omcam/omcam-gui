#include "operation.h"
#include <spdlog/sinks/stdout_color_sinks.h>

using namespace omcam::img;

OperationBase::OperationBase(const std::string& name) : 
    _name(name)
{
    if (spdlog::get(name.c_str()))
        _log = spdlog::get(name.c_str());
    else
        _log = spdlog::stdout_color_mt(name.c_str());
}

std::string OperationBase::getName(void)
{
    return _name;
}

void OperationBase::preExecute(void)
{
    _perf.start();
}

void OperationBase::postExecute(void)
{
    _perf.stop();
}

std::string OperationBase::getPerformanceStatistics()
{
    return _perf.getStatistics();
}

