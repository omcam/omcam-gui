//
// Created by reinis on 03.03.21.
//

#ifndef OMCAM_ACQUISITION_APISERVERTHREAD_H
#define OMCAM_ACQUISITION_APISERVERTHREAD_H

#include <QThread>
#include "api.h"
#include <spdlog/spdlog.h>
#include <QSslConfiguration>
#include "measurement.h"
#include <qhttpengine/server.h>

namespace omcam {
    namespace api {
        class ApiServerThread : public QThread {
            Q_OBJECT
            void run() override;
        public:
            ApiServerThread(const QString& hostname="0.0.0.0", unsigned int port=8000, unsigned int ssl_port=4443);
            ~ApiServerThread() override;
            void addMeasurement(const Measurement& measurement);
            void setSSLConfiguration(QSslConfiguration *conf);
        protected:
            std::shared_ptr<spdlog::logger> _log;
            std::shared_ptr<Api> _api;
            QString      _hostname;
            unsigned int _port;
            unsigned int _ssl_port;
            std::shared_ptr<QHttpEngine::Server>  _server;
            std::shared_ptr<QHttpEngine::Server>  _sslServer;
            QSslConfiguration* _sslConfiguration;
        };

    }
}


#endif //OMCAM_ACQUISITION_APISERVERTHREAD_H
