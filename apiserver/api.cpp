#include "api.h"

omcam::api::Api::Api(std::shared_ptr<spdlog::logger>& log, QObject *parent) :
    QObject(parent),
    _log(log)
{

}

void omcam::api::Api::readMeasurementBuffer(QHttpEngine::Socket *socket) {
    QJsonObject response;
    response.insert("status", QJsonValue("ok"));
    response.insert("count", QJsonValue(measurements.length()));
    QJsonArray measurementsArray;
    uint32_t measurementsCount=0;
    uint32_t maxMeasurements=1000;
    for (auto& m : measurements)
    {
        measurementsArray.append(QJsonObject {
                {"seq", m._sequence_number},
                {"timestamp", m._timestamp},
                {"measurement", m._length_mm},
                {"measurement_width", m._width_mm},
                {"estimate", m._is_estimate}
        });
        measurementsCount++;
        if (measurementsCount == maxMeasurements)
            break;
    }
    response.insert("measurements", measurementsArray);
    _log->info("executing readMeasurementBuffer, returning {} measurements", measurementsCount);
    socket->writeJson(QJsonDocument(response));
}

void omcam::api::Api::clearMeasurementBuffer(QHttpEngine::Socket *socket) {
    QJsonObject response;

    _log->info("executing clearMeasurementBuffer");
    auto map = socket->queryString();
    if (map.contains("seq"))
    {
        auto it = map.find("seq");
        int seq = it->toInt();
        int del = 0;
        _log->info("Erasing measurements with seq <= {}", seq);
        for (auto it=measurements.begin(); it < measurements.end(); it++)
        {
            if (it->_sequence_number <= seq)
            {
                measurements.erase(it);
                del++;
            }
        }
        response.insert("status", QJsonValue("ok"));
        response.insert("measurements_cleared", QJsonValue(del));
    } else {
        response.insert("status", QJsonValue("error"));
        response.insert("error", QJsonValue("seq argument is missing from query string"));
    }

    socket->writeJson(QJsonDocument(response));
}
