#ifndef MEASUREMENT_H
#define MEASUREMENT_H

#include <QObject>
#include <algorithm>
#include <utility>
#include <QDateTime>

namespace omcam {
    namespace api {


class Measurement : public QObject
{
    Q_OBJECT
public:
    Measurement(const Measurement& m) : QObject(NULL),
        _length_mm(m._length_mm),
        _width_mm(m._width_mm),
        _sequence_number(m._sequence_number),
        _timestamp(m._timestamp),
        _is_estimate(m._is_estimate)
    {
    }

    Measurement& operator=(const Measurement& m)
    {
        _length_mm = m._length_mm;
        _sequence_number= m._sequence_number;
        _timestamp= m._timestamp;
        _is_estimate = m._is_estimate;
        return *this;
    }

    explicit Measurement(int length_mm, int sequence_number=0, int timestamp=0, int width_mm=0, bool is_estimate=false) :
        QObject(NULL),
        _length_mm(length_mm),
        _width_mm(width_mm),
        _is_estimate(is_estimate)
    {
        if (sequence_number != 0) {
            _sequence_number = sequence_number;
        } else {
            _sequence_number = sequence_number_s;
            sequence_number_s += 1;
        }
        if (timestamp != 0)
            _timestamp = timestamp;
        else
            _timestamp = QDateTime::currentMSecsSinceEpoch() / 1000;
    }

    int _length_mm;
    int _width_mm;
    int _sequence_number;
    int _timestamp;
    bool _is_estimate;
    static int sequence_number_s;
};

    }
}

#endif // MEASUREMENT_H
