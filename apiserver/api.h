#ifndef API_H
#define API_H

#include <QObject>
#include <qhttpengine/socket.h>
#include <stdint.h>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include "measurement.h"
#include <QJsonValue>
#include <spdlog/logger.h>

namespace omcam {
    namespace api {


class Api : public QObject
{
    Q_OBJECT
public:

    explicit Api(std::shared_ptr<spdlog::logger>& log, QObject *parent = nullptr);
    QList<Measurement> measurements;
public slots:
    void readMeasurementBuffer(QHttpEngine::Socket *socket);

    void clearMeasurementBuffer(QHttpEngine::Socket *socket);

protected:
    std::shared_ptr<spdlog::logger> _log;
};
    }
}
#endif // API_H
