//
// Created by reinis on 03.03.21.
//

#include "ApiServerThread.h"
#include <spdlog/sinks/stdout_color_sinks.h>
#include <qhttpengine/server.h>
#include <qhttpengine/qobjecthandler.h>
#include <QEventLoop>

omcam::api::ApiServerThread::ApiServerThread(const QString& hostname, unsigned int port, unsigned int ssl_port):
        _hostname(hostname),
        _port(port),
        _ssl_port(ssl_port),
        _sslConfiguration(nullptr) {
    _log = spdlog::get("omcam-api");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-api");
    }
    _api = std::make_shared<Api>(_log);
    _log->info("API server created");
}


void omcam::api::ApiServerThread::addMeasurement(const omcam::api::Measurement &measurement) {
   _api->measurements.append(measurement);
}

void omcam::api::ApiServerThread::run() {
   QHttpEngine::QObjectHandler handler;
    handler.registerMethod("readMeasurementBuffer", _api.get(), &Api::readMeasurementBuffer);
    handler.registerMethod("clearMeasurementBuffer", _api.get(), &Api::clearMeasurementBuffer);

    _sslServer = std::make_shared<QHttpEngine::Server>(&handler);
    _server = std::make_shared<QHttpEngine::Server>(&handler);
    if (_sslConfiguration)
        _sslServer->setSslConfiguration(*_sslConfiguration);

    _server->listen(QHostAddress(_hostname), _port);
    _log->info("listening on {}:{}", _hostname.toStdString(), _port);

    _sslServer->listen(QHostAddress(_hostname), _ssl_port);
    _log->info("ssl listening on {}:{}", _hostname.toStdString(), _ssl_port);

    QEventLoop loop;
    loop.exec();
}

omcam::api::ApiServerThread::~ApiServerThread() {
    _api = nullptr;
}

void omcam::api::ApiServerThread::setSSLConfiguration(QSslConfiguration *conf) {
    _sslConfiguration = conf;
}
