OMCam GUI
---------------

This project contains main application of OMCam, the one which is visible to end-user.

Building (cross compilation)
------------------------------------

    mkdir build
    cd build
    source ~/path-to-sdk/environment-setup*
    cmake ..
    make -j8
    

Building (PC version, image from webcam)
------------------------------------------

    mkdir build-pc
    cd build-pc
    cmake -DOMCAM_PLATFORM=pi ..
    make -j8
    ./omcam-gui
    

Useful scripts
-------------------

Build for omcam-pi

`./scripts/build.sh`

Build and deploy for omcam-pi (configure access in `scripts/deploy.sh` first):

`./scripts/deploy.sh`


Built-in debug server
---------------------------

Built-in debug server listens on 0.0.0.0:80 by default. The hostname and port can be changed by providing command line arguments to omcam-gui:

    Usage: ./omcam-gui [options]
    OMCam GUI application

    Options:
      -h, --help                 Displays this help.
      -v, --version              Displays version information.
      --debug-host <debug-host>  Debug server hostname
      --debug-port <debug-port>  Debug server port
      --tmp-dir <tmp-dir>        Temporary directory


