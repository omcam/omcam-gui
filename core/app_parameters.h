#pragma once
#include <string>
#include <stdint.h>
#include <stdbool.h>

namespace omcam {

class AppParameters {
public:
    std::string tmp_dir;
    std::string debug_server_host;
    unsigned int debug_server_port;
    unsigned int api_server_port;

    unsigned int debug_server_ssl_port;
    unsigned int api_server_ssl_port;
};

}
