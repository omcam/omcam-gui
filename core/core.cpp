#include "core/core.hpp"

#include <chrono>

#include <QDir>
#include <QDateTime>
#include <QSslConfiguration>
#include <QSslCertificate>
#include <QSslKey>
#include <sys/stat.h>
#include <opencv2/imgcodecs.hpp>
#include "core/git_version.h"
#include "hal/base/platform.h"

#if OMCAM_PLATFORM == OMCAM_PI
    #include "hal/pi/led-strobe.h"
    #include "hal/pi/distance-sensor.h"
    #include "hal/pc/camera.h"
    #include "hal/pi/gpio.h"
#elif OMCAM_PLATFORM == OMCAM_PC
    #include "hal/pc/led-strobe.h"
    #include "hal/pc/distance-sensor.h"
    #include "hal/pc/camera_vimba.h"
    #include "hal/pc/gpio.h"
#endif


typedef std::chrono::high_resolution_clock Clock;
using namespace omcam;

Core::Core(std::shared_ptr<AppParameters> appParams) :
        QObject(NULL),
        appParameters(appParams),
        _mainTimer(this),
        _ioTimer(this),
        _save_frame(false),
        _save_processed_frame(false),
        _save_frame_continuous(false),
        _acquiredFrameNumber(0),
        _continuousSavePeriod(0.0f),
        _captureInputTriggered(false),
        _savedFrameNumber(0),
        _previousMeasurement(0)
{
    _log = spdlog::get("omcam-core");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-core");
    }
    _log->info("omcam-core git branch {}@{}-{}", GIT_REFSPEC, GIT_SHA1, GIT_LOCAL_CHANGES);

#if OMCAM_PLATFORM == OMCAM_PI
    led = std::shared_ptr<LEDStrobe>(new LEDStrobePi());
    distanceSensor = std::shared_ptr<DistanceSensor>(new DistanceSensorPi());
    camera = std::shared_ptr<Camera>(new CameraPc());
    gpio = std::shared_ptr<hal::GPIO>(new hal::GPIOPi());
    _log->info("running on Pi");
#elif OMCAM_PLATFORM == OMCAM_PC
    led = std::shared_ptr<LEDStrobe>(new LEDStrobePc());
    distanceSensor = std::shared_ptr<DistanceSensor>(new DistanceSensorPc());
    camera = std::shared_ptr<Camera>(new CameraPcVimba());
    gpio = std::shared_ptr<hal::GPIO>(new hal::GPIOPc());
    _log->info("running on PC");
#endif

    //initialize an instance of DebugServerThread
    debugServer = std::make_shared<omcam::debug::DebugServerThread>(
            QString(":/static"), 
            QString(appParameters->debug_server_host.c_str()), 
            appParameters->debug_server_port,
            appParameters->debug_server_ssl_port);

    apiServer = std::make_shared<omcam::api::ApiServerThread>(
            QString(appParameters->debug_server_host.c_str()),
            appParameters->api_server_port,
            appParameters->api_server_ssl_port);

    connect(&_mainTimer, SIGNAL(timeout()), this, SLOT(acquireFrame()));
    connect(&_ioTimer, SIGNAL(timeout()), this, SLOT(processTriggerInput()));
    connect(&_dbgTimer, SIGNAL(timeout()), this, SLOT(printDebugState()));
    memset(&_debug_data, 0, sizeof(_debug_data));
    _debug_data.min_ms_between_trigger_calls = UINT32_MAX - 1;
}

bool Core::initialize()
{
    //create directory for temporary files
    QDir d;
    d.mkpath(appParameters->tmp_dir.c_str());

   
    debugServer->start();
    apiServer->start();
    led->initialize();
#if OMCAM_PLATFORM == OMCAM_PI
    led->set_brightness(0);
    led->enable();

    int delay=10;
    int max = 50;
    for (int i=0; i <= max; i++) {
        led->set_brightness(i);
        QThread::msleep(delay);
    }
    delay = 20;
    for (int i=max; i >= 0; i--) {
        led->set_brightness(i);
        QThread::msleep(delay);
    }

    led->set_brightness(0);
    led->disable();
#endif

    led->set_brightness(50);
    int w = 1280;
    int h = 720;
#if OMCAM_PLATFORM == OMCAM_PC
    w =  1296;
    h =  248;
#endif
    camera->setup(w, h);
    if (!camera->open()) {
        _log->error("could not open camera");
    } else {
        _log->info("camera opened");
    }

    _currentFrame = cv::Mat(camera->getHeight(), camera->getWidth(), CV_8UC3); 

    if (!distanceSensor->open(0x29, "/dev/i2c-1")) {
        _log->error("cannot open distance sensor");
    } else {
        _log->info("distance sensor opened");
        if (!distanceSensor->start_continuous_ranging()) {
            _log->error("cannot start continuous ranging");
        }
    }

    gpio->initialize();

    _opRectification = std::make_shared<omcam::img::Rectification>("rectification", "");
    loadCameraCalibration();
#if OMCAM_PLATFORM == OMCAM_PI
    omcam::img::board_length_measurement_parameters_t params = {
            .threshold_min=50,
            .roi = cv::Rect2i(0, 380, 1280, 150),

            .sf_length_poly_a0 = 7.80551286e+02,
            .sf_length_poly_a1 = 6.05772145e+00,
            .sf_length_poly_a2 = -3.26553064e-03,
            .sf_length_poly_a3 = 1.54930219e-06,

            .sf_width_poly_a0 = 1.84515993e+01,
            .sf_width_poly_a1 = 3.33074439e+00,
            .sf_width_poly_a2 = -6.28582930e-03,
            .sf_width_poly_a3 = 0.0f,
    };
#endif
#if OMCAM_PLATFORM == OMCAM_PC
    omcam::img::board_length_measurement_parameters_t params = {
            .threshold_min=50,
            .roi = cv::Rect2i(0, 0, 1296, 248),
            .sf_length_poly_a0 = 4.49203754e+02,
            .sf_length_poly_a1 = 6.73606563e+00,
            .sf_length_poly_a2 = -4.46918211e-03,
            .sf_length_poly_a3 = 2.18024277e-06,

            .sf_width_poly_a0 = 1.84515993e+01,
            .sf_width_poly_a1 = 3.33074439e+00,
            .sf_width_poly_a2 = -6.28582930e-03,
            .sf_width_poly_a3 = 0.0f,
    };
#endif

    _opBoardLengthMeasurement = std::make_shared<omcam::img::WatershedBoardLengthMeasurement>("blm", params);
    return true;
}


void Core::acquireFrame() 
{
    static auto first_frame_time = Clock::now();
    static auto last_saved_time = Clock::now();
    if (!camera->isOpened()) {
        return;
    }

    auto start_time = Clock::now();
    auto grab_time = Clock::now();
    bool img_captured = camera->captureImage(_currentFrame);
    auto retrieve_time = Clock::now();
    bool input_triggered = false;
    if (img_captured) {
        _captureInputTriggered = false;
        //do the processing of input image
        std::shared_ptr<cv::Mat> rectified = std::make_shared<cv::Mat>();
#if OMCAM_PLATFORM == OMCAM_PC
        cv::Point2f cp(_currentFrame.cols/2.0f, _currentFrame.rows/2.0f);
        float rotAngle = -1.95f;
        auto rotMat = cv::getRotationMatrix2D(cp, rotAngle, 1.0f);
        cv::Mat inputMatRotated;
        cv::warpAffine(_currentFrame, inputMatRotated, rotMat, _currentFrame.size());
        auto inputPtr = std::make_shared<cv::Mat>(inputMatRotated);
#else

        auto inputPtr = std::make_shared<cv::Mat>(_currentFrame);
#endif
        auto outputPtr = std::make_shared<cv::Mat>();
        //_opRectification->execute(inputPtr, rectified);
        _opBoardLengthMeasurement->execute(inputPtr, outputPtr);
        auto results = _opBoardLengthMeasurement->getResults();

        //_save_frame = true;
        input_triggered = true;
        _log->info("Board length measurement status: {}", results.ok);
        _log->info("number of boards detected: {}", results.boards.size());

        bool record_estimated = results.boards.empty() || !results.ok;

        for (auto& b: results.boards) {
            _log->info("board length: {}mm ({}px), width: {}mm ({}px)",
                       b.board_length_mm, b.board_length_px,
                       b.board_width_mm, b.board_width_px);
            if (b.board_length_px < 1260) {
                _log->info("adding board to measurements API");
                auto m = omcam::api::Measurement(b.board_length_mm, 0, 0, b.board_width_mm);
                _log->info("seq={}", m._sequence_number);
                apiServer->addMeasurement(m);
                _previousMeasurement = m;
                _debug_data.num_boards_added++;
            } else {
                _log->debug("ignoring too long board");
                record_estimated = true;
            }
        }
        /*
        if (record_estimated) {
            _log->info("sensor triggered but no board measurement detected, adding estimated measurement");
            auto m = omcam::api::Measurement(_previousMeasurement._length_mm, 0, 0, _previousMeasurement._width_mm, true);
            apiServer->addMeasurement(m);
        }
         */

        debugServer->setImageData("/corners", results.corners_dbg_mat);
        _currentAnnotatedFrame = results.corners_dbg_mat;

        auto process_finished_time = Clock::now();
        emit frameAcquired();
        debugServer->setImageData("/main", _currentFrame);
        float range_mm = 0.0f;
        /*
        if (distanceSensor->continuous_ranging_started()) {
            if (distanceSensor->wait_measurement()) {
                DistanceSensor::range_status_t status = distanceSensor->read_range(range_mm);
                emit distanceSensorReadingReceived(status == DistanceSensor::STATUS_RANGE_VALID, range_mm);
            } else {
                emit distanceSensorTimeout();
            }
        } else {
            emit distanceSensorError();
        }
         */
        if (_save_frame || _save_frame_continuous || _save_processed_frame) {
            auto period_s = (float)std::chrono::duration_cast<std::chrono::seconds>(retrieve_time - last_saved_time).count();
            if (_save_frame || (_save_frame_continuous && period_s >= _continuousSavePeriod) ||
                (_save_processed_frame && input_triggered))
            {
                QDateTime dt = QDateTime::currentDateTime();

                QDir dir(QString("/var/cache/omcam/%1/").arg(dt.toString("yyyy-MM-dd")));
                dir.mkpath(".");

                QString filename = dir.filePath(QString("capture_%1_%2_%3mm.png").arg(
                        dt.toString("hh-mm"), QString::number(_acquiredFrameNumber), QString::number(_previousMeasurement._length_mm)));

                if (cv::imwrite(filename.toStdString(), _currentFrame)) {
                    _savedFrameNumber++;

                    if (_save_frame) {
                        _save_frame = false;
                    }
                    _log->info("frame saved to {}", filename.toStdString());
                    last_saved_time = Clock::now();
                } else {
                    _log->error("failed to save frame {}", filename.toStdString());
                }
            }
        }
        auto save_finished = Clock::now();
        if (input_triggered) {
            auto acquire_ms = std::chrono::duration_cast<std::chrono::milliseconds>(retrieve_time - grab_time).count();
            auto process_ms = std::chrono::duration_cast<std::chrono::milliseconds>(process_finished_time - retrieve_time).count();
            auto save_ms = std::chrono::duration_cast<std::chrono::milliseconds>(save_finished - process_finished_time).count();
            _log->info("acquireFrame perf: acquire_time={}ms, process_time={}ms, save_time={}ms, total={}ms, acquiredFrameNumber={}",
                       acquire_ms, process_ms, save_ms, acquire_ms + process_ms + save_ms, _acquiredFrameNumber);
        }
        _acquiredFrameNumber++;

    }
}

bool Core::loadCameraCalibration() {
    struct stat buffer;
    std::string calibrationFilename = "/etc/omcam/camera_calibration.yml";
    if (_opRectification) {
        if (_opRectification->loadParameters(calibrationFilename)) {
            _log->info("using camera calibration parameters: {}", calibrationFilename);
            return true;
        } else {
            _log->info("failed to load calibration parameters from: {}", calibrationFilename);
        }
    }
    return false;
}

void Core::startImageCapture(float frameRate) {
    _log->info("starting image capture at {} FPS", frameRate);
    _mainTimer.start((int)(1000.0f / frameRate));
    _ioTimer.start((int)(1000.0f / 50));
    _dbgTimer.start(1000 * 10);
}

void Core::terminate() {
    _log->info("stopping server threads...");
    debugServer->quit();
    apiServer->quit();
    if (debugServer->wait(100)) {
        _log->info("debugserver thread stopped");
    } else {
        _log->error("failed to stop debugserver thread");
    }
    if (apiServer->wait(100)) {
        _log->info("apiserver thread stopped");
    } else {
        _log->error("failed to stop apiserver  thread");
    }
    camera->close();
}

bool Core::cameraCalibrationLoaded() {
    return _opRectification->parametersLoaded();

}

bool Core::loadSSLCertificate(QString ssl_certificate, QString ssl_key) {
    _sslConfiguration = std::make_shared<QSslConfiguration>();

    QFile certFile(ssl_certificate);
    QFile keyFile(ssl_key);
    if (!certFile.open(QIODevice::ReadOnly)) {
        _log->error("failed to open SSL certificate file {}", ssl_certificate.toStdString());
        return false;
    }
    if (!keyFile.open(QIODevice::ReadOnly)) {
        _log->error("failed to open SSL key {}", ssl_key.toStdString());
        return false;
    }

    QSslCertificate certificate(&certFile, QSsl::Pem);
    QSslKey sslKey(&keyFile, QSsl::Rsa, QSsl::Pem);

    certFile.close();
    keyFile.close();

    if (sslKey.isNull()) {
        _log->error("failed to load SSL key from file {}", ssl_key.toStdString());
        return false;
    }
    if (certificate.isNull()) {
        _log->error("failed to load SSL certificate from file {}", ssl_certificate.toStdString());
        return false;
    }
    _log->info("Certificate info: {}", certificate.issuerDisplayName().toStdString());

    _sslConfiguration->setPeerVerifyMode(QSslSocket::VerifyNone);
    _sslConfiguration->setLocalCertificate(certificate);
    _sslConfiguration->setPrivateKey(sslKey);

    apiServer->setSSLConfiguration(_sslConfiguration.get());
    debugServer->setSSLConfiguration(_sslConfiguration.get());

    return true;
}

void Core::processTriggerInput() {
    auto now_ms = QDateTime::currentMSecsSinceEpoch();
    if (_debug_data.last_triger_call_ms != 0) {
        uint32_t delta = now_ms - _debug_data.last_triger_call_ms;
        if (delta != 0) {
            if (delta > _debug_data.max_ms_between_trigger_calls)
                _debug_data.max_ms_between_trigger_calls = delta;
            if (delta < _debug_data.min_ms_between_trigger_calls)
                _debug_data.min_ms_between_trigger_calls = delta;
        }
    }
    _debug_data.last_triger_call_ms = now_ms;

    if (gpio->get_input_posedge(hal::IN1)) {
        _debug_data.num_input_triggered++;
        _log->info("triggered");
        _captureInputTriggered = true;
    }
}

void Core::printDebugState() {
    if (_debug_data.last_triger_call_ms != 0) {
        _log->info("debug state: boards_measured={}, input_triggered={}, min_ms_between_trigger={}, max_ms_between_trigger={}",
                   _debug_data.num_boards_added, _debug_data.num_input_triggered,
                   _debug_data.min_ms_between_trigger_calls, _debug_data.max_ms_between_trigger_calls);
    }
}
