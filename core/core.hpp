#pragma once

#include <QObject>
#include <QTimer>

#include <opencv2/core.hpp>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "hal/base/camera.h"
#include "hal/base/distance-sensor.h"
#include "hal/base/led-strobe.h"
#include "hal/base/gpio.h"

#include "core/app_parameters.h"
#include "debugserver/debugserverthread.hpp"
#include "apiserver/measurement.h"
#include "apiserver/ApiServerThread.h"
#include "pipeline/pipeline.h"
#include "pipeline/ops/Rectification.h"
#include "pipeline/ops/WatershedBoardLengthMeasurement.h"
#include <QSslConfiguration>

namespace omcam {

class Core : public QObject {
    Q_OBJECT

public:
    Core(std::shared_ptr<AppParameters> appParams);

    bool initialize();

    void processFrame(cv::Mat& mat, cv::Point2f& main_point);
    void getPointRange(cv::Mat& mat, cv::Point2f& point);

    std::shared_ptr<DistanceSensor>  distanceSensor;
    std::shared_ptr<LEDStrobe>       led;
    std::shared_ptr<Camera>          camera;
    std::shared_ptr<hal::GPIO>       gpio;
    std::shared_ptr<omcam::debug::DebugServerThread> debugServer;
    std::shared_ptr<omcam::api::ApiServerThread> apiServer;
    std::shared_ptr<omcam::AppParameters> appParameters;
    
    //save frames to /tmp/
    void startDumpingFrames() { _save_frame_continuous = true; }
    void stopDumpingFrames() { _save_frame_continuous = false; }
    void startSavingProcessedFrames() { _save_processed_frame = true;}
    void stopSavingProcessedFrames() { _save_processed_frame = false;}
    void setContinuousDumpingPeriod(float per) { _continuousSavePeriod = per; }
    float getContinuousDumpingPeriod() const { return _continuousSavePeriod; }

    cv::Mat& getCurrentFrame(void) { return _currentFrame; }
    cv::Mat& getCurrentAnnotatedFrame(void) { return _currentAnnotatedFrame; }
    uint32_t getNumSavedFrames() const { return _savedFrameNumber; }

    bool loadCameraCalibration();
    bool cameraCalibrationLoaded();
    void startImageCapture(float frameRate);
    void terminate();
    bool loadSSLCertificate(QString ssl_certificate, QString ssl_key);
    omcam::api::Measurement getCurrentMeasurement() const { return _previousMeasurement; }

protected slots:
    void acquireFrame();
    void processTriggerInput();
    void printDebugState();
signals:
    void distanceSensorReadingReceived(bool measurementValid, float measurement);
    void distanceSensorTimeout();
    void distanceSensorError();
    
    void frameAcquired();

protected:
    QTimer      _mainTimer;
    QTimer      _ioTimer;
    QTimer      _dbgTimer;
    cv::Mat     _currentFrame;
    cv::Mat     _currentAnnotatedFrame;
    std::shared_ptr<spdlog::logger> _log;
    uint32_t    _acquiredFrameNumber;
    uint32_t    _savedFrameNumber;
    bool        _save_frame;
    bool        _save_frame_continuous;

    //whether to save frames where board was detected?
    bool        _save_processed_frame;
    float       _continuousSavePeriod;

    bool        _captureInputTriggered;
    std::shared_ptr<omcam::img::WatershedBoardLengthMeasurement> _opBoardLengthMeasurement;
    std::shared_ptr<omcam::img::Rectification> _opRectification;
    std::shared_ptr<QSslConfiguration> _sslConfiguration;

    omcam::api::Measurement _previousMeasurement;

    struct {
        uint32_t num_boards_added;
        uint32_t num_input_triggered;
        uint32_t min_ms_between_trigger_calls;
        uint32_t max_ms_between_trigger_calls;

        qint64   last_triger_call_ms;
    } _debug_data;
};
}
