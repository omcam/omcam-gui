#pragma once

#include <iostream>

/**
 * A bit nicer version of assert- prints filename, location and message
 */
#ifndef NDEBUG
#   define oassert(Expr, Msg) \
    __O_assert(#Expr, Expr, __FILE__, __LINE__, Msg)
#else
#   define oassert(Expr, Msg) ;
#endif


void __O_assert(const char* expr_str, bool expr, const char* file, int line, const char* msg);
