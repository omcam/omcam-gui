#!/usr/bin/env bash
set -e
set -v
TARGET=grain1-zt
USER=kadikis
#TARGET=192.168.0.236
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null && pwd )"
echo "Copying $DIR to $TARGET:omcam-gui"
ssh $USER@$TARGET "sudo systemctl stop omcam-gui"
rsync -az --info=progress2 --exclude='cmake-build*' --exclude='.idea' --exclude='tests/images*' $DIR $TARGET:
ssh $USER@$TARGET 'mkdir -p omcam-gui/build && cd omcam-gui/build && cmake .. -DOMCAM_PLATFORM=pc && make -j4'
ssh $USER@$TARGET "sudo systemctl start omcam-gui"
