#pragma once
#include <QMainWindow>
#include <QLabel>
#include <QPushButton>
#include <QStackedWidget>
#include <QCloseEvent>
#include <opencv2/core.hpp>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "ui/widgets/appwidget.hpp"
#include "ui/widgets/settingswidget.hpp"
#include "ui/widgets/menuwidget.hpp"
#include "ui/widgets/gpiowidget.hpp"
#include "ui/widgets/cameracalibrationwidget.h"
#include "core/core.hpp"
#include "imageview.hpp"

namespace omcam {

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        MainWindow(std::shared_ptr<omcam::Core> core);
        void create();
        void closeEvent(QCloseEvent* evt) override;
protected slots:
        void captureBtnPressed();
        void captureBtnReleased();
        void ledBtnClicked();
        void updateFrame();
        void menuButtonClicked();
        void distanceReading(bool measurement_valid, float measurement);
        void menuWidgetSelected(QWidget* widget);
        void exitHandler();
protected:
    QLabel      _width_label;
    QLabel      _height_label;
    QLabel      _distance_label;
    QPushButton _capture_btn;
    QPushButton _reset_btn;
    QPushButton _led_btn;
    
    QPushButton _menuButton;
    QLabel      _titleLabel;

    ui::AppWidget*  _appWidget;
    ui::SettingsWidget*  _settingsWidget;
    ui::MenuWidget*      _menuWidget;
    ui::GPIOWidget*      _gpioWidget;
    ui::CameraCalibrationWidget*      _camcalibWidget;

    QStackedWidget* _stackedWidget;

    std::shared_ptr<spdlog::logger> _log;
    std::shared_ptr<omcam::Core> _core;
};

}
