#include "ui/mainwindow.hpp"
#include <QtWidgets>
#include <QNetworkInterface>
#include <QDebug>
#include <QImage>
#include <QIcon>
#include <QHBoxLayout>
#include <QDateTime>
#include <QDir>
#include <QThread>
#include <QSpacerItem>
#include "ui/widgets/appwidget.hpp"
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc/types_c.h>
#include "core/git_version.h"

#include "hal/base/platform.h"




using namespace omcam;


typedef std::chrono::high_resolution_clock Clock;

int num_frames=0;

MainWindow::MainWindow(std::shared_ptr<Core> core) :
    QMainWindow(),
    _core(core),
    _appWidget(new ui::AppWidget(this)),
    _settingsWidget(new ui::SettingsWidget(this)),
    _menuWidget(new ui::MenuWidget(this)),
    _gpioWidget(new ui::GPIOWidget(core, this)),
    _camcalibWidget(new ui::CameraCalibrationWidget(core, this)),
    _stackedWidget(new QStackedWidget(this))
{
    _log = spdlog::get("omcam-gui");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-gui");
    }

    QVBoxLayout *mainLayout = new QVBoxLayout();
    QHBoxLayout *titleBarLayout = new QHBoxLayout();
    _titleLabel.setText("OMCam measurement");
    titleBarLayout->addWidget(&_titleLabel);
    titleBarLayout->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Fixed));
    _menuButton.setText("");
    _menuButton.setIcon(QIcon(":/icons/bars-solid.svg.png"));
    _menuButton.setIconSize(QSize(16, 16));

    titleBarLayout->addWidget(&_menuButton);
    
    mainLayout->addLayout(titleBarLayout);
#if OMCAM_PLATFORM == OMCAM_PI
    _stackedWidget->setMaximumWidth(240);
#endif
    _stackedWidget->addWidget(this->_appWidget);
    _stackedWidget->addWidget(this->_settingsWidget);
    _stackedWidget->addWidget(this->_menuWidget);
    _stackedWidget->addWidget(this->_gpioWidget);
    _stackedWidget->addWidget(this->_camcalibWidget);
    _menuWidget->registerWidget(_settingsWidget, "Settings");
    _menuWidget->registerWidget(_gpioWidget, "Inputs/outputs");
    _menuWidget->registerWidget(_camcalibWidget, "Camera calibration");
    connect(_menuWidget, SIGNAL(widgetSelected(QWidget*)), this, SLOT(menuWidgetSelected(QWidget*)));

    mainLayout->addWidget(_stackedWidget);

    QHBoxLayout *wh_layout = new QHBoxLayout();

    _width_label.setText("Width: N/A");
    wh_layout->addWidget(&_width_label);

    _height_label.setText("Height: N/A");
    wh_layout->addWidget(&_height_label);
    mainLayout->addLayout(wh_layout);

    QHBoxLayout *dist_layout = new QHBoxLayout();
    _distance_label.setText("Distance: N/A");
    dist_layout->addWidget(&_distance_label);
    mainLayout->addLayout(dist_layout);

    /*
    QHBoxLayout *btn_layout = new QHBoxLayout();

    _capture_btn.setText("Capture");
    btn_layout->addWidget(&_capture_btn); 
    _led_btn.setText("LED");

    _menuButton.setFocusPolicy(Qt::NoFocus);
    _led_btn.setFocusPolicy(Qt::NoFocus);
    _capture_btn.setFocusPolicy(Qt::NoFocus);

    btn_layout->addWidget(&_led_btn);
    mainLayout->addLayout(btn_layout);
    */
    mainLayout->setContentsMargins(0, 0, 0, 0);
    QWidget* window = new QWidget(this);
    window->setLayout(mainLayout);

    setCentralWidget(window);

    connect(&_menuButton, SIGNAL(clicked()), this, SLOT(menuButtonClicked()));
    connect(&_capture_btn, SIGNAL(pressed()), this, SLOT(captureBtnPressed()));
    connect(&_capture_btn, SIGNAL(released()), this, SLOT(captureBtnReleased()));
    connect(&_led_btn, SIGNAL(clicked()), this, SLOT(ledBtnClicked())); 
    connect(_core.get(), SIGNAL(frameAcquired()), this, SLOT(updateFrame()));
    connect(_core.get(), SIGNAL(distanceSensorReadingReceived(bool, float)), this, SLOT(distanceReading(bool, float)));

#if OMCAM_PLATFORM == OMCAM_PI
    //hide cursor on Pi
    QApplication::setOverrideCursor(Qt::BlankCursor);
#endif
}

void MainWindow::create()
{
}

void MainWindow::captureBtnPressed(void)
{
    _core->startDumpingFrames();
}

void MainWindow::captureBtnReleased(void)
{
    _core->stopDumpingFrames();
}

void MainWindow::ledBtnClicked(void) {
    _log->info("toggling LED from UI");
    _core->led->toggle();
}

void MainWindow::updateFrame(void) {
    cv::Mat& frame = _core->getCurrentAnnotatedFrame();
    if (frame.empty())
        frame = _core->getCurrentFrame();

    _appWidget->imageView->setCVImage(frame);
    auto m = _core->getCurrentMeasurement();
    _width_label.setText(QString("Length: %1 mm").arg(m._length_mm));
    _height_label.setText(QString("Width: %1 mm").arg(m._width_mm));
}

void MainWindow::menuButtonClicked(void) {
    if (_stackedWidget->currentWidget() == _appWidget) {
        _stackedWidget->setCurrentWidget(_menuWidget);
    } else {
        _stackedWidget->setCurrentWidget(_appWidget);
    }
}

void MainWindow::distanceReading(bool measurement_valid, float measurement) {
    if (measurement_valid)
        _distance_label.setText(QString("Distance: %1 mm").arg(QString::number(measurement)));
    else
        _distance_label.setText("Distance: N/A");
}

void MainWindow::menuWidgetSelected(QWidget *widget) {
    _stackedWidget->setCurrentWidget(widget);
}

void MainWindow::closeEvent(QCloseEvent *evt) {
    _core->terminate();
    evt->accept();
}

void MainWindow::exitHandler() {
    _core->terminate();
}
