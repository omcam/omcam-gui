#ifndef IMAGECONVERTER_H
#define IMAGECONVERTER_H

#include <opencv2/core/core.hpp>
#include <QImage>
#include <QPixmap>

class ImageConverter
{
public:
    ImageConverter();
    static QImage convertImage(const cv::Mat& mat);
    static QPixmap toPixmap(const cv::Mat& mat);
private:
    static QVector<QRgb> colorTable;
};

#endif // IMAGECONVERTER_H
