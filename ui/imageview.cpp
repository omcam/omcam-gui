#include "imageview.hpp"
#include <opencv2/imgproc.hpp>
#include <QMouseEvent>
#include <opencv2/imgproc.hpp>
#include "imageconverter.h"

ImageView::ImageView(QWidget* parent) : QGraphicsView(NULL, parent)
{
    setScene(&_scene);    
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

}

void ImageView::mousePressEvent(QMouseEvent* evt) {
    QPointF scene_point = mapToScene(evt->x(), evt->y());

    printf("evt x=%d, y=%d\n", evt->x(), evt->y());

    if (scene_point.x() > 0.0f && scene_point.y() > 0.0f 
            && scene_point.x() < 240 && scene_point.y() < 180) 
    {
        const float scale = 640 / 240.0f;

        printf("scene x=%.2f, y=%.2f\n", scene_point.x(), scene_point.y());

        emit pointSelected(scene_point.x() * scale , scene_point.y() * scale);
    }

    QGraphicsView::mousePressEvent(evt);
}

void ImageView::setCVImage(cv::Mat& mat) {

    cv::Mat transformed;
    int w = 240;
    int h = 180;
#if OMCAM_PLATFORM == OMCAM_PC
    w = this->width();
    h = (int)(float(w) / mat.cols * mat.rows);
#endif
    cv::resize(mat, transformed, cv::Size(w, h));

    _scene.clear();
    _scene.addPixmap(ImageConverter::toPixmap(transformed));
}
