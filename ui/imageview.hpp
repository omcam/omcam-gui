#pragma once

#include <QGraphicsView>
#include <QGraphicsScene>
#include <opencv2/core.hpp>

class ImageView : public QGraphicsView {
    Q_OBJECT

public:
    ImageView(QWidget* parent = NULL);
    void setCVImage(cv::Mat& mat);

signals:
    void pointSelected(float x, float y); 

protected:

    void mousePressEvent(QMouseEvent *event);
    QGraphicsScene _scene;

};
