#pragma once

#include <QWidget>
#include <QLabel>
#include <QMap>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QCheckBox>
#include <spdlog/spdlog.h>
#include "core/core.hpp"

/**
 * GPIOWidget is showing menu of other settings/apps
 */
namespace omcam {
namespace ui {

class GPIOWidget : public QWidget {
    Q_OBJECT

    public:
        GPIOWidget(std::shared_ptr<Core> core, QWidget* parent = NULL);

    public slots:
        void updateStatus();
        void forceCheckboxStateChanged(int state);
    protected:
        QVBoxLayout*            _mainLayout;
        std::shared_ptr<spdlog::logger> _log;

        void createInputWidgets(unsigned int i);
        void createInputForceWidgets(unsigned int i);
        void updateInputWidgets(unsigned int i);
        void createOutputWidgets(unsigned int i);

        QMap<QString, QCheckBox*> _inputStatusCheckboxes;
        QHBoxLayout*             _inputStatusLayout;
        QHBoxLayout*             _inputForceLayout;
        QMap<QCheckBox*, omcam::hal::input_t> _inputForceCheckboxes;
        QMap<QString, QCheckBox*> _outputStatusCheckboxes;
        std::shared_ptr<Core>   _core;
};

}}