//
// Created by reinis on 22.02.21.
//

#pragma once

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QMap>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QCheckBox>
#include <spdlog/spdlog.h>
#include "core/core.hpp"

namespace omcam {
    namespace ui {
        class CameraCalibrationWidget : public QWidget {
        Q_OBJECT
        public:
            explicit CameraCalibrationWidget(std::shared_ptr<Core> core, QWidget *parent = NULL);

        protected slots:
            void onAcquireCalibrationDataClicked();
            void onUpdateTimer();

        protected:
            QLabel _calibrationStatus;
            QLabel _calibrationFrameCount;
            QPushButton _acquireCalibrationData;
            bool _acquiringData;
            QVBoxLayout *_mainLayout;
            std::shared_ptr<spdlog::logger> _log;
            std::shared_ptr<Core> _core;
            void updateCalibrationFrameCount(uint32_t num_frames);
            QTimer _updateTimer;
        };


    }
}
