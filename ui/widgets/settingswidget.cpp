#include "ui/widgets/settingswidget.hpp"

#include <QVBoxLayout>
#include <QFormLayout>
#include <QNetworkInterface>
#include "core/git_version.h"

omcam::ui::SettingsWidget::SettingsWidget(QWidget* parent) :

    QWidget(parent)
{
    QVBoxLayout *mainLayout = new QVBoxLayout();
    
    QFormLayout* formLayout = new QFormLayout();

    formLayout->addRow(new QLabel("Version: "), new QLabel(QString("%1").arg(GIT_REFSPEC)));

    
    auto list = QNetworkInterface::allInterfaces();
    for (auto interface: list)
    {
        if (interface.name() == "lo")
            continue;
        QLabel* value = new QLabel();
        formLayout->addRow(new QLabel(QString("%1:").arg(interface.name())), value);
        _interfaceLabels.insert(interface.name(), value);
    }


    mainLayout->addLayout(formLayout);

    setLayout(mainLayout);

    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    setLayout(mainLayout);

    connect(&_updateTimer, SIGNAL(timeout()), this, SLOT(updateStatus()));
    _updateTimer.start(1000);
    updateStatus();
}

void omcam::ui::SettingsWidget::updateInterfaceStatus()
{
    auto list = QNetworkInterface::allInterfaces();
    for (auto interface: list)
    {
        if (_interfaceLabels.contains(interface.name()))
        {
            QLabel* label = _interfaceLabels[interface.name()];
            if ((interface.flags() & QNetworkInterface::IsRunning) && 
                    (interface.flags() & QNetworkInterface::IsUp)) {
                auto ips = interface.addressEntries();
                if (ips.size() > 0) {
                    label->setText(QString("up, %1").arg(ips[0].ip().toString()));
                } else {
                    label->setText("up, no IP");
                }
            } else {
                label->setText("down");
            }
        }
    }
}

void omcam::ui::SettingsWidget::updateStatus()
{
    updateInterfaceStatus();
}
