#pragma once

#include <QWidget>
#include "ui/imageview.hpp"

/**
 * AppWidget is responsible for the camera image preview and related output data
 */
namespace omcam {
namespace ui {

class AppWidget : public QWidget {
Q_OBJECT

public:
    AppWidget(QWidget *parent = NULL);

    ImageView *imageView;
};

}}
