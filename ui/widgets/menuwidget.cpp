#include "ui/widgets/menuwidget.hpp"
#include <QPushButton>
#include <spdlog/sinks/stdout_color_sinks.h>

using namespace omcam::ui;

MenuWidget::MenuWidget(QWidget* parent) :
    QWidget(parent),
    _mainLayout(new QVBoxLayout())
{
    _log = spdlog::get("omcam-menu");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-menu");
    }

    _mainLayout->setSpacing(0);
    _mainLayout->setContentsMargins(5, 0, 5, 0);
    setLayout(_mainLayout);
    _log->info("initialized");

}

void MenuWidget::updateStatus()
{

}

void MenuWidget::menuItemClicked()
{
    QPushButton *obj = qobject_cast<QPushButton*>(sender());
    if (obj != NULL)
    {
        QString title = obj->text();

        if (_menuItems.contains(title))
        {
            _log->info("emitting widgetSelected for item '{}'", title.toStdString());
            emit widgetSelected(_menuItems[title]);
        }
        else
        {
            _log->error("could not find menu item with title '{}'", title.toStdString());
        }
    }

}

void MenuWidget::registerWidget(QWidget *widget, QString title) {
    if (!_menuItems.contains(title)) {
        _menuItems[title] = widget;

        QPushButton* btn = new QPushButton();
        btn->setText(title);
        connect(btn, SIGNAL(clicked()), this, SLOT(menuItemClicked()));
        _mainLayout->addWidget(btn);
        _log->info("registered menu item '{}'", title.toStdString());
    } else {
        _log->error("menu already contains item with title '{}'", title.toStdString());
    }
}
