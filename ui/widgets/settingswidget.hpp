#pragma once

#include <QWidget>
#include <QLabel>
#include <QMap>
#include <QTimer>

/**
 * SettingsWidget is showing status/settings
 */
namespace omcam {
namespace ui {

    class SettingsWidget : public QWidget {
    Q_OBJECT

    public:
        SettingsWidget(QWidget *parent = NULL);

    public slots:

        void updateStatus();

    protected:
        QMap<QString, QLabel *> _interfaceLabels;

        void updateInterfaceStatus();

        QTimer _updateTimer;
    };

}}
