#pragma once

#include <QWidget>
#include <QLabel>
#include <QMap>
#include <QTimer>
#include <QVBoxLayout>
#include <spdlog/spdlog.h>

/**
 * MenuWidget is showing menu of other settings/apps
 */
namespace omcam {
namespace ui {

class MenuWidget : public QWidget {
    Q_OBJECT

public:
    MenuWidget(QWidget* parent = NULL);

    void registerWidget(QWidget* widget, QString title);

public slots:
    void updateStatus();
    void menuItemClicked();

signals:
    void widgetSelected(QWidget* widget);

protected:
    QMap<QString, QWidget*> _menuItems;
    QVBoxLayout*            _mainLayout;
    std::shared_ptr<spdlog::logger> _log;
};
}}
