#include "ui/widgets/appwidget.hpp"

#include <QVBoxLayout>

omcam::ui::AppWidget::AppWidget(QWidget* parent) :
    QWidget(parent),
    imageView(new ImageView(this))
{
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(imageView);
    mainLayout->setSpacing(0);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    setLayout(mainLayout);
}
