#include "ui/widgets/gpiowidget.hpp"
#include "hal/base/gpio.h"

#include <QTimer>
using namespace omcam;

omcam::ui::GPIOWidget::GPIOWidget(std::shared_ptr<Core> core, QWidget *parent) :
    QWidget(parent),
    _core(core),
    _mainLayout(new QVBoxLayout()),
    _inputStatusLayout(new QHBoxLayout()),
    _inputForceLayout(new QHBoxLayout())
{
    auto isl = new QLabel("Input status:");
    _mainLayout->addWidget(isl);
    _mainLayout->addLayout(_inputStatusLayout);
    for (unsigned int i = 0; i < hal::IN_END; i++) {
        createInputWidgets(i);
    }
    _mainLayout->addWidget(new QLabel("Input force:"));
    _mainLayout->addLayout(_inputForceLayout);
    for (unsigned int i = 0; i < hal::IN_END; i++) {
        createInputForceWidgets(i);
    }

    auto timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(updateStatus()));
    timer->start(100);
    setLayout(_mainLayout);
}

void omcam::ui::GPIOWidget::updateStatus() {
    for (unsigned int i = 0; i < hal::IN_END; i++) {
        updateInputWidgets(i);
    }
}

void ui::GPIOWidget::createInputWidgets(unsigned int i) {
    QString title = hal::INPUT_NAMES[i];
    QCheckBox* cb = new QCheckBox(title);
    _inputStatusLayout->addWidget(cb);
    _inputStatusCheckboxes[title] = cb;
}

void ui::GPIOWidget::updateInputWidgets(unsigned int i) {
    QString title = hal::INPUT_NAMES[i];

    _inputStatusCheckboxes[title]->setChecked(
            _core->gpio->get_input_state((hal::input_t)i) == hal::HIGH
            );
}

void ui::GPIOWidget::createInputForceWidgets(unsigned int i) {
    QString title = QString("%1").arg(hal::INPUT_NAMES[i]);
    auto cb = new QCheckBox(title);
    cb->setTristate();
    cb->setCheckState(Qt::PartiallyChecked);
    connect(cb, SIGNAL(stateChanged(int)), this,
            SLOT(forceCheckboxStateChanged(int)));
    _inputForceLayout->addWidget(cb);
    _inputForceCheckboxes[cb] = (hal::input_t)(i);
}

void ui::GPIOWidget::forceCheckboxStateChanged(int) {
    auto iter = _inputForceCheckboxes.constBegin();
    while (iter != _inputForceCheckboxes.constEnd()) {
        auto state = iter.key()->checkState();
        auto input = (hal::input_t)iter.value();
        if (state == Qt::Checked) {
            _core->gpio->start_forcing(input, omcam::hal::HIGH);
        } else if (state == Qt::Unchecked) {
            _core->gpio->start_forcing(input, omcam::hal::LOW);
        } else {
            _core->gpio->stop_forcing(input);
        }

        ++iter;
    }
}
