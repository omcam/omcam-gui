//
// Created by reinis on 22.02.21.
//

#include "cameracalibrationwidget.h"
QString ACQUIRE_BUTTON_TEXT[] = {"Acquire calibration images", "Stop acquiring"};
omcam::ui::CameraCalibrationWidget::CameraCalibrationWidget(std::shared_ptr<Core> core, QWidget *parent) :
        QWidget(parent),
        _acquiringData(false),
        _mainLayout(new QVBoxLayout()),
        _core(core)
{
    _log = spdlog::get("omcam-camcalib");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-camcalib");
    }
    _log->info("camera calibration widget loaded");
    if (core->cameraCalibrationLoaded())
        _calibrationStatus.setText("Status: calibrated");
    else
        _calibrationStatus.setText("Status: not calibrated");
    _mainLayout->addWidget(&_calibrationStatus);

    /** Setup acquire calibration data button **/
    _acquireCalibrationData.setText(ACQUIRE_BUTTON_TEXT[0]);
    _mainLayout->addWidget(&_acquireCalibrationData);
    updateCalibrationFrameCount(0);
    _mainLayout->addWidget(&_calibrationFrameCount);
    connect(&_acquireCalibrationData, &QPushButton::clicked,
            this, &CameraCalibrationWidget::onAcquireCalibrationDataClicked);
    connect(&_updateTimer, &QTimer::timeout, this, &CameraCalibrationWidget::onUpdateTimer);
    setLayout(_mainLayout);
}

void omcam::ui::CameraCalibrationWidget::onAcquireCalibrationDataClicked() {

    _acquiringData = !_acquiringData;
    if (_acquiringData) {
        _core->startDumpingFrames();
        _updateTimer.start(500);
    } else {
        _core->stopDumpingFrames();
        _updateTimer.stop();
    }

    _core->setContinuousDumpingPeriod(1.0f);
    _acquireCalibrationData.setText(ACQUIRE_BUTTON_TEXT[(int)_acquiringData]);
}

void omcam::ui::CameraCalibrationWidget::updateCalibrationFrameCount(uint32_t num_frames) {
    _calibrationFrameCount.setText(QString("Calibration frames saved: %1").arg(QString::number(num_frames)));
}

void omcam::ui::CameraCalibrationWidget::onUpdateTimer() {
    updateCalibrationFrameCount(_core->getNumSavedFrames());
}

