#pragma once

#include <QThread>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <opencv2/core.hpp>
#include "debugserver/mjpegserver.hpp"
#include <QSslConfiguration>

namespace omcam {
namespace debug {

class DebugServerThread : public QThread {
    Q_OBJECT
    void run() override;

    public:
        DebugServerThread(QString www_dir=":/static",
                          QString hostname="0.0.0.0",
                          unsigned int port = 8080,
                          unsigned int ssl_port = 8043);
        void setImageData(QString key, cv::Mat& img);
        bool haveClientsConnected();
        void setSSLConfiguration(QSslConfiguration* conf);
    protected:
        QString      _www_dir;
        QString      _hostname;
        unsigned int _port;
        unsigned int _sslPort;
        std::shared_ptr<spdlog::logger> _log;
        MJPEGServer* _mjpeg_server;
        MJPEGServer* _ssl_mjpeg_server;
        QSslConfiguration* _sslConfiguration;
};

}
}
