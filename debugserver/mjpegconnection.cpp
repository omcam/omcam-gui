#include "debugserver/mjpegconnection.hpp"

#include <QTcpSocket>
#include <QtGlobal>

using namespace omcam::debug;

MJPEGConnection::MJPEGConnection(
            qintptr socketDescriptor, 
            MJPEGRegistry* registry,
            QSslConfiguration* ssl_conf,
            std::shared_ptr<spdlog::logger> log,
            QObject *parent) : 
        QObject(parent), 
        _socket_fd(socketDescriptor), 
        _registry(registry), 
        _headers_read(false), 
        _abort_connection(false), 
        _log(log),
        _socket(nullptr),
        _sslSocket(nullptr),
        _response_headers_written(false),
        _timer(new QTimer(this)),
        _sslConfiguration(ssl_conf)
{
    if (_sslConfiguration) {
        _sslSocket = new QSslSocket(this);
        if (!_sslSocket->setSocketDescriptor(_socket_fd)) {
            _log->error(_sslSocket->errorString().toStdString());
        }

        _sslSocket->setSslConfiguration(*_sslConfiguration);
        _sslSocket->startServerEncryption();
        _socket = (QTcpSocket*)_sslSocket;
    } else {
        _socket = new QTcpSocket(this);
        if (!_socket->setSocketDescriptor(_socket_fd)) {
            _log->error(_socket->errorString().toStdString());
        }
   }

    connect(_socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(_socket, SIGNAL(disconnected()), this, SLOT(onClose()));

    connect(_timer, SIGNAL(timeout()), this, SLOT(onTimerTick()));
}

void MJPEGConnection::onReadyRead()
{
    if (!_headers_read)
        parseHeaders(_socket->readAll());

    if (_headers_read) {
        if (!_response_headers_written) {
            writeResponseHeaders();
            _response_headers_written = true;
            _timer->start(100);
        }
    }
}

void MJPEGConnection::onClose()
{
    if (_headers_read)
        _log->info("stopping mjpeg request: {}, fd={}", _image_key.toStdString(), _socket_fd);

    deleteLater();
}

void MJPEGConnection::onTimerTick()
{
    writeResponse();
}

void MJPEGConnection::writeResponseHeaders()
{
    QByteArray headers = ("HTTP/1.0 200 OK\r\n" \
            "Server: OMCam debug server MJPEG server\r\n" \
            "Cache-Control: no-cache\r\n" \
            "Cache-Control: private\r\n" \
            "Content-Type: multipart/x-mixed-replace;boundary=--boundary\r\n\r\n");
    _socket->write(headers);
    _socket->flush();
}

void MJPEGConnection::writeResponse()
{
    QByteArray image_data;
    if (_registry->getImageData(_image_key, image_data)) {
        _log->debug("writing MJPEGConnection response");
        QString boundary_header = QString("--boundary\r\n" \
                "Content-Type: image/jpeg\r\n" \
                "Content-Length: %1\r\n\r\n").arg(QString::number(image_data.length()));
        _socket->write(boundary_header.toUtf8());
        _socket->write(image_data);
        _socket->flush();
        _registry->updateClientReadTime();
    }
}

void MJPEGConnection::parseHeaders(QByteArray data)
{
    _request_data.append(QString::fromUtf8(data));
    _log->debug("parsing headers: '{}'", _request_data.toStdString());
    if (_request_data.contains("\r\n\r\n")) 
    {
        //we have full request headers, parse them
        QStringList request_lines = _request_data.split("\r\n");
        if (request_lines.count() == 0 ) {
            _log->error("could not split request into lines");
            _abort_connection = true;
            return;
        }
        QStringList first_line = request_lines[0].split(" ");
        if (first_line.count() != 3) {
            _log->error("could not split request line into parts");
            return;
        }
        _image_key = first_line[1];

        _log->info("handling mjpeg request: {}, fd={}",  _image_key.toStdString(), _socket_fd);
        _registry->updateClientReadTime();
        _headers_read = true;
    }
}
