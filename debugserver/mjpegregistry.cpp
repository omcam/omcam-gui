#include "debugserver/mjpegregistry.hpp"

using namespace omcam::debug;

bool MJPEGRegistry::getImageData(QString name, QByteArray& data) 
{
    _image_data_mutex.lock();
     
    bool ret = _image_data.contains(name);
    if (ret)
        data = _image_data[name];

    _image_data_mutex.unlock();
    return ret;
}

void MJPEGRegistry::setImageData(QString name, QByteArray data)
{
    _image_data_mutex.lock();

    _image_data[name] = data;

    _image_data_mutex.unlock();
}

void MJPEGRegistry::updateClientReadTime() {
    _lastClientReadTime = std::chrono::system_clock::now();
    _lastClientReadTimeInitialized = true;
}

bool MJPEGRegistry::haveClientsConnected() {
    if (_lastClientReadTimeInitialized) {
        auto now = std::chrono::system_clock::now();
        auto delta_s = std::chrono::duration_cast<std::chrono::seconds>(now - _lastClientReadTime).count();
        return delta_s < 20;
    } else {
        return false;
    }
}

MJPEGRegistry::MJPEGRegistry() :
    _lastClientReadTime(std::chrono::system_clock::now()),
    _lastClientReadTimeInitialized(false)
{

}
