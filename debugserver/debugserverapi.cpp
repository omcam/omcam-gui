#include "debugserver/debugserverapi.hpp"
#include <QDir>
#include <QFile>
#include "core/git_version.h"

DebugServerApi::DebugServerApi(QString www_dir) : 
    QObject(),
    _www_dir(www_dir)
{
    _log = spdlog::get("omcam-debugapi");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-debugapi");
    }
}

void DebugServerApi::renderIndex(QHttpEngine::Socket *socket) 
{
    _log->debug("rendering debugserver index");
    //read index template
    QDir dir(_www_dir);
    QString filename = dir.filePath("index.html.tmpl");
    QFile f(filename);

    if (!f.open(QIODevice::ReadOnly)) {
        _log->error("could not open {}", filename.toStdString());
        socket->writeHeaders();
        socket->close();
        return;
    }

    QString html = f.readAll();
    html.replace("{{GIT_SHA1}}", GIT_SHA1);
    html.replace("{{GIT_REFSPEC}}", GIT_REFSPEC);
    html.replace("{{GIT_LOCAL_CHANGES}}", GIT_LOCAL_CHANGES);
    socket->writeHeaders();
    socket->write(html.toUtf8());
    socket->close();
    _log->debug("rendering index done");
}
