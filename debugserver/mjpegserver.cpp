#include "debugserver/mjpegserver.hpp"
#include "debugserver/mjpegconnection.hpp"

using namespace omcam::debug;

MJPEGServer::MJPEGServer(QObject *parent) :
    QTcpServer(parent),
    _sslConfiguration(nullptr)
{
    _log = spdlog::get("omcam-mjpeg");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-mjpeg");
    }
}

void MJPEGServer::incomingConnection(qintptr socketDescriptor)
{
    MJPEGConnection *connection = new MJPEGConnection(socketDescriptor, &registry,
                                                      _sslConfiguration, _log, this);
}
