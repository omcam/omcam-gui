#pragma once

#include <QThread>
#include <QTcpServer>

#include "debugserver/mjpegregistry.hpp"

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <QSslConfiguration>

namespace omcam {
namespace debug {

class MJPEGServer : public QTcpServer
{
    Q_OBJECT

public:
    MJPEGServer(QObject *parent = 0);
    MJPEGRegistry   registry;
    void setSSLConfiguration(QSslConfiguration* conf) { _sslConfiguration = conf; }

protected:
    void incomingConnection(qintptr socketDescriptor) override;
    std::shared_ptr<spdlog::logger> _log;
    QSslConfiguration* _sslConfiguration;
};

}
}
