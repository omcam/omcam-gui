#pragma once

#include <QObject>
#include <qhttpengine/server.h>
#include <qhttpengine/socket.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>


class DebugServerApi : public QObject
{
    Q_OBJECT

public:
    DebugServerApi(QString www_dir);
public slots:
    void renderIndex(QHttpEngine::Socket *socket);

protected:
    QString _www_dir;
    std::shared_ptr<spdlog::logger> _log;
};
