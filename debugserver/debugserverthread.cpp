#include "debugserverthread.hpp"

#include <QEventLoop>

#include <qhttpengine/filesystemhandler.h>
#include <qhttpengine/server.h>
#include <qhttpengine/qobjecthandler.h>
#include "debugserverapi.hpp"
#include <opencv2/imgcodecs.hpp>

using namespace omcam::debug;

DebugServerThread::DebugServerThread(QString www_dir, QString hostname, unsigned int port, unsigned int ssl_port) :
    _www_dir(www_dir),
    _hostname(hostname),
    _port(port),
    _sslPort(ssl_port),
    _mjpeg_server(nullptr),
    _ssl_mjpeg_server(nullptr),
    _sslConfiguration(nullptr)
{
    _log = spdlog::get("omcam-debugserver");
    if (!_log) {
        _log = spdlog::stdout_color_mt("omcam-debugserver");
    }

}

void DebugServerThread::run()
{
    _mjpeg_server = new MJPEGServer();

    DebugServerApi api(_www_dir);
    QHttpEngine::QObjectHandler handler;
    handler.registerMethod("index.html", &api, &DebugServerApi::renderIndex);
    handler.addRedirect(QRegExp("^$"), "/index.html");

    QHttpEngine::FilesystemHandler static_handler(_www_dir);
    handler.addSubHandler(QRegExp("^static/"), &static_handler);

    QHttpEngine::Server server(&handler);

    QHostAddress addr(_hostname);
    quint16 port(_port);

    if (!server.listen(addr, port)) {
        _log->error("could not listen on http://{}:{}", _hostname.toStdString(), _port);
    } else {
        _log->info("listening on http://{}:{}", _hostname.toStdString(), _port);
    }
    
    quint16 mjpeg_port(_port + 1);
    if (!_mjpeg_server->listen(addr, mjpeg_port)) {
        _log->error("could not start MJPEG server on http://{}:{}", _hostname.toStdString(), mjpeg_port);
    } else {
        _log->info("MJPEG server listening on http://{}:{}", _hostname.toStdString(), mjpeg_port);
    }

    QHttpEngine::Server* _sslServer = nullptr;
    if (_sslConfiguration) {
        _sslServer = new QHttpEngine::Server(&handler);
        _sslServer->setSslConfiguration(*_sslConfiguration);
        if (!_sslServer->listen(addr, quint16(_sslPort))) {
            _log->error("could not listen on https://{}:{}", _hostname.toStdString(), _sslPort);
        } else {
            _log->info("listening on https://{}:{}", _hostname.toStdString(), _sslPort);
        }

        quint16 ssl_mjpeg_port(_sslPort+ 1);
        _ssl_mjpeg_server = new MJPEGServer();
        _ssl_mjpeg_server->setSSLConfiguration(_sslConfiguration);
        if (!_ssl_mjpeg_server->listen(addr, ssl_mjpeg_port)) {
            _log->error("could not start MJPEG server on https://{}:{}", _hostname.toStdString(), ssl_mjpeg_port);
        } else {
            _log->info("MJPEG server listening on https://{}:{}", _hostname.toStdString(), ssl_mjpeg_port);
        }
    }

    QEventLoop loop;
    loop.exec();
}

void DebugServerThread::setImageData(QString key, cv::Mat& img)
{
    if (!_mjpeg_server || !haveClientsConnected())
        return;

    std::vector<uchar> buf;
    std::vector<int> param(2);
    param[0] = cv::IMWRITE_JPEG_QUALITY;
    param[1] = 80;//default(95) 0-100
    cv::imencode(".jpg", img, buf, param);
   
    _mjpeg_server->registry.setImageData(key, QByteArray(reinterpret_cast<const char*>(buf.data()), buf.size()));
    if (_ssl_mjpeg_server)
        _ssl_mjpeg_server->registry.setImageData(key, QByteArray(reinterpret_cast<const char*>(buf.data()), buf.size()));
}

void DebugServerThread::setSSLConfiguration(QSslConfiguration *conf) {
    _sslConfiguration = conf;
}

bool DebugServerThread::haveClientsConnected() {
    return (_mjpeg_server && _mjpeg_server->registry.haveClientsConnected()) ||
           (_ssl_mjpeg_server && _ssl_mjpeg_server->registry.haveClientsConnected());
}

