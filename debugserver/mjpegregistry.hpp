#pragma once

#include <QObject>
#include <QMutex>
#include <QMap>
#include <QByteArray>
#include <QString>
#include <chrono>

namespace omcam {
namespace debug {

class MJPEGRegistry : QObject {
    Q_OBJECT

    public:
        MJPEGRegistry();
        bool getImageData(QString name, QByteArray& data);
        void setImageData(QString name, QByteArray data);
        void updateClientReadTime();

        bool haveClientsConnected();
    protected:
        QMap<QString,QByteArray> _image_data;
        QMutex                    _image_data_mutex;
        std::chrono::time_point<std::chrono::system_clock>  _lastClientReadTime;
        bool _lastClientReadTimeInitialized;
};

}
}
