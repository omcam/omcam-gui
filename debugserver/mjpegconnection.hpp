#pragma once

#include <stdbool.h>

#include <QObject>
#include <QTcpSocket>
#include "debugserver/mjpegregistry.hpp"
#include <spdlog/spdlog.h>
#include <QTimer>
#include <QSslSocket>
#include <QSslConfiguration>

namespace omcam {
namespace debug {

class MJPEGConnection: public QObject
{
    Q_OBJECT

public:
    MJPEGConnection(qintptr socketDescriptor, MJPEGRegistry* registry, QSslConfiguration* ssl_conf,
                    std::shared_ptr<spdlog::logger> log, QObject *parent);
    
signals:
    void error(QTcpSocket::SocketError socketError);

private slots:
    void onReadyRead();
    void onTimerTick();
    void onClose();
protected:
    void parseHeaders(QByteArray data);

    void writeResponseHeaders();
    void writeResponse();

    qintptr         _socket_fd;
    MJPEGRegistry*  _registry;
    
    bool            _headers_read;
    bool            _abort_connection;
    bool            _response_headers_written;
    QString         _image_key;
    QString         _request_data;
    std::shared_ptr<spdlog::logger> _log;
    QTcpSocket*     _socket;
    QSslSocket*     _sslSocket;
    QTimer*         _timer;

    QSslConfiguration* _sslConfiguration;
};

}
}
