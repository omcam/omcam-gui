#!/usr/bin/env bash
TARGET=omcam-002
#TARGET=192.168.0.236
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null && pwd )"
ssh root@$TARGET "systemctl stop omcam-gui"
scp $DIR/cmake-build-pi/omcam-gui root@$TARGET:/usr/bin/
ssh root@$TARGET "systemctl start omcam-gui"
