#!/usr/bin/env bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ~/Sync/omcam/sdk/environment-setup-cortexa7hf-neon-vfpv4-poky-linux-gnueabi
cd $DIR/../build
make -j4
