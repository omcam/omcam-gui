#!/usr/bin/env bash
#set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/config.sh

ssh $USER@$HOST "systemctl stop omcam-gui && killall -q gdbserver"
scp omcam-gui $USER@$HOST:
ssh $USER@$HOST "QT_QPA_PLATFORM=linuxfb:fb=/dev/fb1 gdbserver :1234 omcam-gui"
