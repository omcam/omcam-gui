#!/usr/bin/env bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ~/Sync/omcam/sdk/environment-setup-cortexa7t2hf-neon-vfpv4-poky-linux-gnueabi
cd $DIR/../build
make -j4

source $DIR/config.sh

set -v
ssh $USER@$HOST "systemctl stop omcam-gui"
scp omcam-gui $USER@$HOST:/usr/bin/
ssh $USER@$HOST "systemctl start omcam-gui"
