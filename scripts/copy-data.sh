#!/usr/bin/env bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/config.sh

TARGET=$1
if [[ -e $TARGET ]]
then
    echo "Specify target dir"
    exit 1
fi
echo "Copying data from omcam to $TARGET"
scp -r $USER@$HOST:/tmp/omcam/ $TARGET
