#!/usr/bin/env bash
set -e
set -v
source ~/Sync/omcam/sdk/environment-setup-cortexa7hf-neon-vfpv4-poky-linux-gnueabi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR/../build
make -j4
source $DIR/config.sh
ssh $USER@$HOST "pkill -9 omcam-gui;sleep 1;true"
scp omcam-gui $USER@$HOST:

ssh $USER@$HOST "QT_QPA_PLATFORM=linuxfb:fb=/dev/fb1 nohup ./omcam-gui > /dev/null 2>&1 &"
