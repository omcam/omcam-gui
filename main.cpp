#include <QApplication>
#include "ui/mainwindow.hpp"
#include <QCommandLineParser>
#include <QFont>
#include <spdlog/spdlog.h>

#include "core/core.hpp"
#include <QFile>
#include <signal.h>

using namespace omcam;

void osSignalHandler(int sig) {
    QCoreApplication::quit();
}

int main(int argc, char **argv)
{
    QApplication app (argc, argv);
    QFile stylesheet(":/style/stylesheet.css");
    stylesheet.open(QIODevice::ReadOnly);
    app.setStyleSheet(stylesheet.readAll());

    QCoreApplication::setApplicationName("omcam-gui");
    QCoreApplication::setApplicationVersion("0.1");

    //create command line options
    QCommandLineParser parser;
    parser.setApplicationDescription("OMCam GUI application");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption debug(QStringList() << "d" << "debug", "Additional debug output.");
    parser.addOption(debug);

    QCommandLineOption debug_host("debug-host", "Debug server hostname.", "debug-host", "0.0.0.0");
    parser.addOption(debug_host);

    QCommandLineOption framerate("framerate", "Framerate",
            "framerate", "20");
    parser.addOption(framerate);

    QCommandLineOption debug_port("debug-port", "Debug server port.", "debug-port", "80");
    parser.addOption(debug_port);

    QCommandLineOption debug_ssl_port("debug-ssl-port", "Debug server SSL port.", "debug-ssl-port", "443");
    parser.addOption(debug_ssl_port);

    QCommandLineOption api_port("api-port", "API server port.", "api-port", "8000");
    parser.addOption(api_port);

    QCommandLineOption api_ssl_port("api-ssl-port", "API server SSL port.", "api-ssl-port", "4443");
    parser.addOption(api_ssl_port);

    QCommandLineOption tmp_dir("tmp-dir", "Temporary directory.", "tmp-dir", "/tmp/omcam");
    parser.addOption(tmp_dir);

    QCommandLineOption ssl_cert("ssl-certificate", "SSL certificate file.", "ssl-certificate", "");
    parser.addOption(ssl_cert);

    QCommandLineOption ssl_key("ssl-key", "SSL private key file.", "ssl-key", "");
    parser.addOption(ssl_key);



    //parse the options
    parser.process(app);
    if (parser.isSet(debug))
        spdlog::set_level(spdlog::level::debug);

    auto ap = std::make_shared<omcam::AppParameters>();
    ap->debug_server_host = parser.value(debug_host).toStdString();
    ap->debug_server_port = parser.value(debug_port).toUInt();
    ap->debug_server_ssl_port = parser.value(debug_ssl_port).toUInt();
    ap->api_server_port = parser.value(api_port).toUInt();
    ap->api_server_ssl_port = parser.value(api_ssl_port).toUInt();
    ap->tmp_dir = parser.value(tmp_dir).toStdString();
    auto fps = parser.value(framerate).toFloat();
    auto core = std::make_shared<omcam::Core>(ap);

    //core->setContinuousDumpingPeriod(30.0f);
    core->startSavingProcessedFrames();

    if (parser.value(ssl_cert).length() > 0  && parser.value(ssl_key).length() > 0) {
        if (!core->loadSSLCertificate(parser.value(ssl_cert), parser.value(ssl_key))) {
            return -1;
        }
    }
    core->initialize();
    core->startImageCapture(fps);

    MainWindow w(core);
    QObject::connect(&app, SIGNAL(aboutToQuit()), &w, SLOT(exitHandler()));
    w.create();

    w.show();
   /*
#if OMCAM_PLATFORM == OMCAM_PI
    w.showFullScreen();
#else
    w.show();
#endif
*/
    signal(SIGINT, osSignalHandler);
    signal(SIGTERM, osSignalHandler);
    return app.exec();
}
